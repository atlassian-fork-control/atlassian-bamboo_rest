# Puppet Module to Manage Atlassian Bamboo

#### Table of Contents

1. [Overview](#overview)
2. [Module Description](#module-description)
3. [Requirements](#Requirements)
4. [Usage](#usage)
5. [Reference](#reference)
6. [Limitations](#limitations)
7. [Development](#development)

## Overview

Puppet module to manage [Bamboo](https://www.atlassian.com/software/bamboo) via REST endpoints ([JIRA issue tracker](https://ecosystem.atlassian.net/projects/BPM)).

Important Note:

 * The REST APIs used by this Puppet module are not officially supported by Atlassian. They might change in the future
 * All the dark features used by this Puppet module are not officially supported by Atlassian.


## Module Description

This module contains types and provides to manage Bamboo administrative configurations, thus it provides the possibility
to manage Bamboo automatically.

It includes types to manage:

 * Bamboo general administration configurations. http://bambooUrl/admin/configure!default.action
 * Build monitoring. http://bambooUrl/admin/buildHangingDetection!read.action
 * Enable/disable audit log. http://bambooUrl/admin/viewGlobalAuditLog.action
 * Default concurrent build. http://bambooUrl/admin/viewConcurrentBuildConfig.action
 * Security Settings. https://bambooUrl/admin/viewSecurity.action
 * Global Variables. https://bambooUrl/admin/admin/configureGlobalVariables!default.action
 * Bamboo agents. https://bambooUrl/admin/agent/configureAgents!default.action
 * Build Expiry. https://bambooUrl/admin/buildExpiry!read.action
 * UPM settings. https://bambooUrl/plugins/latest/settings
 * User Repositories. https://bambooUrl/admin/userRepositories.action
 * Groups and permissions. https://bambooUrl/admin/viewGlobalPermissions.action
 * Role permissions. https://bambooUrl/admin/viewGlobalPermissions.action
 * Quarantine Configuration. https://bambooUrl/admin/editQuarantineSettings.action
 * Bamboo Elastic Configuration. http://bambooUrl/admin/elastic/viewElasticConfig.action
 * Artifact Handlers. https://bambooUrl/admin/configureArtifactHandlers.action
 * Shared Credential. https://bambooUrl/admin/credentials/configureSharedCredentials.action
 * Mail Server. https://bambooUrl/admin/viewMailServer.action
 * IM Server. https://bambooUrl/admin/instantmessagingserver/viewInstantMessagingServer.action
 * Remote Agent Support. https://bambooUrl/admin/agent/configureAgents!default.action
 * Dark Features Support. https://bambooUrl/rest/admin/latest/darkFeatures

## Requirements

The module doesn't have any dependencies on other Puppet modules. But it is using Ruby libraries that may not be
contained in the default installation. Currently, the following libraries are expected to be installed; the module won't
install or manage them in any way (see the Gemfile for more information):

* json
* mime-types
* openssl

Depending on the version of Ruby you're using, some of them are shipped by default (e.g. `json`) whereas others need
to be installed via Puppet or in a bootstrap process like

```
package { 'openssl':
  ensure   => installed,
  provider => gem,
}
```

Obviously it would be a great improvement if the module itself could provide a manifest to install and manage its
dependencies. Any contributions are welcome!

Limitation: At the moment the module doesn't handle the provision-from-scratch use-case very well and will cause a
couple of warnings during the resource discovery phase as the Ruby gems may not be available. As long as the gems are
installed before the Puppet resources are used, everything should be fine.

## Usage

The REST endpoints used by this module are currently protected behind a dark feature switch. To enable them, please add
`-Datlassian.darkfeature.bamboo.experimental.rest.admin.enabled=true` to your Bamboo start script and restart Bamboo.
To verify they are enabled, try `https://bambooUrl/rest/admin/latest/config/general` to see if it gives a 200.

Since this module manages Bamboo via REST endpoints, a configuration file which contains Bamboo login information is required.
The file `bamboo_rest.conf` should exist in Puppet config directory, e.g. `/etc/puppet`.

```
bamboo_base_url: http://localhost:6990/bamboo

# The Puppet module supports changing Bamboo user repositories type. E.g. when provisioning Bamboo from scratch, use the
# built-in admin/admin credentials to log in and change user repositories to Crowd or customised LDAP configuration. If you
# want to use it (the bamboo_user_repositories type), you need to provide two sets of credentials, the local credentials and
# Crowd ones. The Puppet module will automatically detect which credentials to use by reading Bamboo configuration. Thus you
# should also configure the Bamboo home directory.

# Bamboo local credential used when invoking REST endpoints
admin_username: admin
admin_password: admin

# Crowd credentials used when invoking REST endpoints
crowd_admin_username: admin
crowd_admin_password: admin

# Bamboo home directory
bamboo_home_dir: /data/bamboo-data/

# timeout in seconds for reading response from Bamboo
# connection_timeout: 20
# timeout in seconds for opening connection to Bamboo
# connection_open_timeout: 20

# retry times when checking Bamboo health status
# health_check_retries: 3
# timeout in seconds when reanding health status from Bamboo
# health_check_timeout: 20
```
Note: Most Bamboo administrative REST endpoints require System Admin or Restricted Admin permission, so the credentials
should at least be a Restricted Admin.

Note: The namevar of the Puppet resource must be 'current' (as shown in all the examples below) unless used by the resource
(as is the case for global variables, agents, groups and permissions, role permissions). Other types will not work.

### Bamboo General Configuration

```
bamboo_administration_general_configuration { 'current':
  instance_name             => 'bamboo',
  base_url                  => 'http://example.com',
  broker_url                => 'tcp://0.0.0.0:54663?wireFormat.maxInactivityDuration=300000',
  broker_client_url         => 'failover:(tcp://example.com:54663?wireFormat.maxInactivityDuration=300000)?initialReconnectDelay=15000&maxReconnectAttempts=10',
  dashboard_page_size       => 50,
  branch_detection_interval => 300,
  gravatar_enabled          => true
  gravatar_server_url       => 'https://secure.gravatar.com/avatar/',
  gzip_compression_enabled  => false
}
```
Note:

 * Restricted admin required
 * After changing `broker_url` or `broker_client_url`, Bamboo needs to restart to pick up the change. However, this type does not force a restart.
 * Partial update is supported, you can only specify a subset of the properties above

### Audit Log
```
bamboo_audit_log { 'current':
  audit_log_enabled => true
}
```
Note:

 * Restricted admin required

### Build Concurrency
```
bamboo_build_concurrency{ 'current':
  build_concurrency_enabled         => true,
  default_concurrency_builds_number => 10
}
```
Note:

 * Restricted admin required
 * `build_concurrency_enabled` is mandatory. If `default_concurrency_builds_number` is not specified and build concurrency is enabled, Bamboo will use whatever is previously stored.

### Expiry

```
bamboo_build_expiry { 'current':
  builds_to_keep           => 4,
  period                   => 'days',
  cron_expression          => '0 0 0 ? * *',
  deployments_to_keep      => 2,
  duration                 => 4,
  expire_artifacts         => false,
  expire_logs              => true,
  expire_results           => false,
  labels_to_exclude        => '',
  maximum_ignored_log_size => -1,
}
```
Note:

 * cron_expression is in the Quartz Chrontrigger format (not the normal unix format)
 * cron_expression is not validated in this module, nor via Bamboo when updating via REST. An invalid cron will be accepted, but will not work (and the Bamboo UI will state 'invalid cron expression')
 * duration or builds_to_keep must be non-zero
 * period can be 'days', 'weeks' or 'months'

### Build Monitoring
```
bamboo_build_monitoring { 'current':
  build_monitoring_enabled            => true,
  build_time_multiplier_default       => 2.5,
  log_quiet_minutes_time_default      => 10,
  build_queue_minutes_timeout_default => 60
}
```
Note:

 * Restricted admin required
 * `build_monitoring_enabled` is required. If it is enabled and other attributes not specified, Bamboo will use whatever is previously stored.

### Security Settings

```
bamboo_security_settings { 'current':
  read_only_external_user_management                  => true,
  sign_up_enabled                                     => true,
  sign_up_enabled_captcha                             => false,
  display_contact_details_enabled                     => true,
  restricted_administrator_role_enabled               => true,
  brute_force_protection_enabled                      => true,
  brute_force_protection_login_attempts               => 3,
  xsrf_protection_enabled                             => true,
  xsrf_protection_disabled_for_http_get               => true,
  resolve_artifacts_content_type_by_extension_enabled => false,
}
```
Note:

 * System Admin required

### Global Variables

```
bamboo_global_variable{ 'foo':
  ensure => 'present',
  value  => 'bar',
}
```

Note:

 * Restricted admin required

### Agents

```
bamboo_agent{ 'local':        # Supported agent types: local, remote and elastic.
  enabled => false,
}
```
Note:

 * Restricted admin required.
 * If enabled is set to false in manifest, Puppet will make sure the type of agents are all disabled.
 * If one type of agent does not have any instance and enabled is set to true, Puppet will think it is currently disabled and will try to enable agents of that type. Though no actual http request will be sent.
 * If there is any agent of one type is enabled, Puppet will think that type of agents are enabled, and do nothing if enabled is set to true in manifest.
 * If agents of one type are all disabled, Puppet will think this type of agents are disabled, and enable all if enabled is set to true.

### UPM Settings

```
bamboo_upm_settings { 'current':
  auto_update_enabled            => false,
  email_notification_enabled     => false,
  marketplace_connection_enabled => true,
  private_listings_enabled       => true,
  request_addon_enabled          => false,
}
```

Note:

 * Restricted admin required
 * `auto_update_enabled`: Add-on updates selected by Atlassian will be automatically downloaded and installed.
 * `email_notification_enabled`: Disabling this will prevent users and administrators from receiving add-on notifications via email.
 * `marketplace_connection_enabled`: Enables connection to marketplace. Warning: Disabling the connection prevents you from finding new add-ons or receiving free updates to your installed add-ons.
 * `private_listings_enabled`: Enables private listings to install and license add-ons that aren't publicly available.
 * `request_addon_enabled`: You'll receive a notification whenever an end-user requests an add-on. View these requests by selecting the Most requested filter.


### Groups and permissions

```
bamboo_group { 'bamboo_admin':
  permissions => ['ACCESS', 'CREATE_PLAN', 'CREATE_REPOSITORY', 'RESTRICTED_ADMIN', 'ADMIN'],
}
```
Note:

 * System Admin required
 * It will create the group if it does not exists and make sure it has the right permissions.
 * Supported permission: `ACCESS`, `CREATE_PLAN`, `CREATE_REPOSITORY`, `RESTRICTED_ADMIN`, `ADMIN`.

### Role permissions

```
bamboo_role_permissions { 'ROLE_USER':
  permissions => ['ACCESS'],
}
```
Note:

 * System Admin required
 * It only support 2 roles, ROLE_USER for logged in users and ROLE_ANONYMOUS for anonymous users
 * ROLE_USER cannot have more than `ACCESS`, `CREATE_PLAN` and `CREATE_REPOSITORY` permissions, ROLE_ANONYMOUS cannot have more than `ACCESS` permission.


### User Repositories
```
bamboo_user_repositories { 'current':
  type                   => 'Crowd',
  server_url             => 'https://crowd.atlassian.com/crowd',
  application_name       => 'bamboo-app',
  application_password   => 'hunter2',
  cache_refresh_interval => 60,
  require                => Bamboo_group['crowd-bamboo-admin'],
}
```
Note:

 * System Admin required
 * `type`: allowed values: `Local`, `Crowd` and `Custom`.
 * `server_url`: Crowd or Jira url. Only needed when `type` is `Crowd`
 * `application_name`: Bamboo authenticates with JIRA or Crowd using this name.
 * `application_password`: Bamboo authenticates with JIRA or Crowd using this password.
 * `cache_refresh_interval`: Time (in minutes) between updating the list of users and groups.
 * Make sure at least one group in crowd has system admin permission.

### Quarantine Configuration
```
bamboo_quarantine { 'current':
  quarantine_tests_enabled => true
}
```
Note:

 * Restricted admin required

### Elastic Configuration
```
bamboo_elastic_configuration { 'current':
    enabled => true,
    access_key                          => 'VALID_AWS_ACCESS_KEY',
    secret_key                          => 'VALID_AWS_SECRET',
    region                              => 'US_WEST_1',
    private_key_file                    => '/var/pkey.file',
    certificate_file                    => '/var/certificate.file',
    upload_aws_identifier               => true,
    max_elastic_instances               => 5,
    allocate_public_ip_to_vpc_instances => true,
    im_type                             => 'Custom',
    im_idle_shutdown_delay_minutes      => 10,
    im_allowed_non_bamboo_instances     => 7,
    im_max_num_to_start                 => 30,
    im_queued_build_threshold           => 10,
    im_elastic_queued_build_threshold   => 5,
    im_average_queue_time               => 200,
    termination_enabled                 => true,
    termination_shutdown_delay          => 300
}
```
Note:

 * System Admin required
 * `access_key` and `secret_key` must be valid aws credentials
 * `region` must be a valid aws regions supported by Bamboo. Acceptable values are:
   * `US_EAST_1`
   * `US_WEST_1`
   * `US_WEST_2`
   * `EU_WEST_1`
   * `EU_CENTRAL_1`
   * `ASIA_PACIFIC_SE_1`
   * `ASIA_PACIFIC_SE_2`
   * `ASIA_PACIFIC_NE_1`
   * `SOUTH_AMERICA_1`
   * `US_GOV_W1`
   * `CN_NORTH_1`
 * `private_key_file` and `certificate_file` are locations of the respective files on the Bamboo server. They must be readable by Bamboo
 * `im_type` can be `Disabled`, `Custom`, `Passive`, `Aggressive` or `Default`
 * if `im_type` is not `Custom`, no other im_xxxx properties can be set
 * `im_elastic_queued_build_threshold` can not be greater than `im_queued_build_threshold`
 * `termination_shutdown_delay` can only be set if `termination_enabled` is true

### Artifact Handlers
```
bamboo_artifact_handler { 'agent_local':
  artifact_storage_location    => 'opt',
  non_shared_artifacts_enabled => false,
  shared_artifacts_enabled     => false,
  provider                     => agent_local,
}

bamboo_artifact_handler { 'bamboo_remote':
  non_shared_artifacts_enabled => false,
  shared_artifacts_enabled     => false,
  provider                     => bamboo_remote,
}

bamboo_artifact_handler { 's3':
  access_key_id                => 'abc',
  bucket_name                  => 'my bucket',
  region                       => 'us-east-1',
  bucket_path                  => 'bamboo-artifacts',
  max_artifact_file_count      => '100',
  non_shared_artifacts_enabled => true,
  secret_access_key            => 'abcd',
  shared_artifacts_enabled     => 'false',
  provider                     => s3,
}

bamboo_artifact_handler { 'server_local':
  non_shared_artifacts_enabled => false,
  shared_artifacts_enabled     => true,
  provider                     => server_local,
}
```

Note:
 * Restricted admin required
 * To manage artifact handlers, Bamboo must enable the artifact handlers dark feature. Please add `-Dbamboo.darkfeature.artifacthandlers=true` to your Bamboo start script and restart Bamboo
 * Artifact handlers can be extended by plugins, only 4 types are supported: agent_local, server_local, bamboo_remote and s3
 * Different types of artifact handlers have their own provider. It's recommended to add provider type in your manifest.

### Shared Credentials
```
bamboo_shared_credential { 'bamboosshcredential':
  ensure         => 'present',
  ssh_key        => 'addbcd',
  ssh_passphrase => 'brrarr',
}
```

Note:
 * Restricted admin required
 * Different types of shared credentials can be provided via Bamboo plugins. We currently only support ssh shared credential
 * Bamboo does not enforce uniqueness of shared credential name, If there are duplicated names, the Puppet type might not work properly.

### Mail Server
```
bamboo_mail_server { 'current':
  from                            => 'bamboo',
  from_address                    => 'bamboo@atlassian.a.c',
  subject_prefix                  => '[sbac]',
  email_settings                  => 'SMTP',
  smtp_server                     => 'mail_server',
  smtp_port                       => 25,
  smtp_username                   => 'foo',
  smtp_password                   => 'bar',
  tls_enabled                     => false,
  precedence_bulk_header_excluded => true,
}
```

Note:
 * System Admin required
 * Because of the way that Bamboo Mail Server REST endpoint works, `puppet resource bamboo_mail_server` will fail if mail server hasn't been configured before.

### Remote IM Server
```
bamboo_im_server { 'current':
  ensure        => 'present',
  host          => 'mail.atlassian.com',
  port          => 2222,
  resource_name => 'bamboo',
  username      => 'foo',
  password      => 'bar',
  tls_enabled   => true,
}
```

Note:
 * Restricted admin required
 * Since Bamboo allows only one IM server configured, we only allow resource title to be 'current'


### Remote Agent Support
```
bamboo_remote_agent_support { 'current':
  remote_agents_supported => false,
}
```

Note:
 * Restricted admin required

### Dark Feature Support
```
bamboo_dark_feature { 'bamboo.final.stages':
  ensure => present,
}
```

Note:
 * Restricted admin required
 * Only dynamic dark features are supported (not the ones needed to be specified as Java sysprops)

## Reference


## Limitations

This modules relies on Bamboo REST endpoints which are only availabie since 5.9.0. It won't work with previous Bamboo versions.

The module has been tested with various Ruby and Puppet versions.

Supported Ruby versions:

 * Ruby 1.8.7
 * Ruby 1.9.3
 * Ruby 2.0.0
 * Ruby 2.4.2

Supported Puppet Versions:

 * Puppet 3.5
 * Puppet 3.6
 * Puppet 3.7
 * Puppet 4.6
 * Puppet 5.3

It won't work with Puppet 3.4 because of [PUP-1470](https://tickets.puppetlabs.com/browse/PUP-1470).

For password values in Bamboo, a salted hash is returned by Bamboo REST endpoints. The relative Puppet type hashes the value
in manifest and compares the hash values. For a Bamboo global variable, the Puppet type first checks if the variable name contains
the keyword 'password' or 'secret' (case insensitive). If so, it uses the hash mechanism. Otherwise, it just compares the raw
values. When updating password values, they are not hashed. You should rely on an HTTPS connection to protect your sensitive data.

## Development

This project is still new and might not work properly (both Puppet module and Bamboo REST endpoints). If you see any problem, please
[open an issue](https://ecosystem.atlassian.net/projects/BPM). Pull requests are welcome!

## Release Notes

We are still adding more REST endpoints to Bamboo as well as developing new types/providers, please only use released version of the module.
Development version might require REST endpoints have not been released in Bamboo.
