require 'spec_helper'

describe Puppet::Type.type(:bamboo_build_concurrency).provider(:ruby) do |variable|
  describe '.map_config_to_resource_hash' do
    let (:build_concurrency) do
      {
        'buildConcurrencyEnabled'       => true,
        'defaultConcurrentBuildsNumber' => 3,
      }
    end

    let(:resource_hash) do
      described_class::map_config_to_resource_hash(build_concurrency)
    end

    specify { expect(resource_hash[:build_concurrency_enabled]).to eq :true }
    specify { expect(resource_hash[:default_concurrent_builds_number]).to eq 3}

  end

  describe '#map_resource_to_config' do
    let(:instance) do
      instance = described_class.new()
      resource = {
          :build_concurrency_enabled => :true,
          :default_concurrent_builds_number => 5,
      }
      instance.resource = resource
      instance
    end

    let(:build_concurrency) do
      instance.map_resource_hash_to_config
    end

    specify { expect(build_concurrency['buildConcurrencyEnabled']).to eq :true }
    specify { expect(build_concurrency['defaultConcurrentBuildsNumber']).to eq 5 }
  end
end