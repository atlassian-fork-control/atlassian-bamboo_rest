require 'spec_helper'

describe Puppet::Type.type(:bamboo_group).provider(:ruby) do

  let(:provider) do
    provider = described_class.new
  end

  let(:resource) do
    resource = {:name => "groupName"}
    provider.resource = resource
  end

  describe '#delete' do
    specify 'should fail when invoking delete method' do
      expect{provider.delete}.to raise_error(NoMethodError)
    end
  end

  describe '#exists?' do
    specify 'should pass the right parameter to REST module' do
      expect(Bamboo::Rest).to receive(:get_bamboo_settings).with('/rest/admin/latest/security/groups?name=' + resource[:name])
      provider.exists?
    end
  end

  describe '#create' do
    specify 'should just create group if no permissions specified' do
      expect(Bamboo::Rest).to receive(:create_bamboo_resource).with('/rest/admin/latest/security/groups', {"name" => resource[:name]})
      expect(Bamboo::Rest).not_to receive(:update_bamboo_settings).with(:any_parameters)
      provider.create
    end

    context 'empty permissions specified' do
      let(:provider) {provider = described_class.new}
      let(:resource) do
        resource = {:name => "groupName", :permissions => []}
        provider.resource = resource
      end
      it 'should just create group' do
        expect(Bamboo::Rest).to receive(:create_bamboo_resource).with('/rest/admin/latest/security/groups', {"name" => resource[:name]})
        expect(Bamboo::Rest).not_to receive(:update_bamboo_settings).with(:any_parameters)
        provider.create
      end
    end

    context 'non-empty permissions specified' do
      let(:provider) {provider = described_class.new}
      let(:resource) do
        resource = {:name => "groupName", :permissions => ['ACCESS', 'CREATE_PLAN']}
        provider.resource = resource
      end

      it 'should create group and update permissions' do
        expect(Bamboo::Rest).to receive(:create_bamboo_resource).with('/rest/admin/latest/security/groups', {"name" => resource[:name]})
        config = {'name' => 'groupName', 'permissions' => ['ACCESS', 'CREATE_PLAN']}
        expect(Bamboo::Rest).to receive(:update_bamboo_settings).with('/rest/admin/latest/permissions/groups', config, 'post')
        provider.create
      end
    end
  end

  describe '#flush' do
    context 'no property change' do
      let(:provider) { provider = described_class.new }

      it 'should not update bamboo resource' do
        expect(Bamboo::Rest).not_to receive(:update_bamboo_settings).with(:any_parameters)
        provider.flush
      end
    end

    context 'property changed' do
      let(:provider) do
        provider = described_class.new(
          {
              :name        => 'groupName',
              :permissions => ['ACCESS', 'CREATE_PLAN'],
          }
        )
      end

      it 'should update bamboo resource with correct params' do
        provider.permissions = ['ACCESS', 'CREATE_PLAN']
        config = {'name' => 'groupName', 'permissions' => ['ACCESS', 'CREATE_PLAN']}
        expect(provider).to receive(:map_resource_to_group_permissions_config).and_return(config)
        expect(Bamboo::Rest).to receive(:update_bamboo_settings).with("/rest/admin/latest/permissions/groups", config, 'post')
        provider.flush
      end
    end
  end

  describe '#permissions' do
    specify 'should invoke bamboo rest endpoint to retrieve permissions' do
      expect(Bamboo::Rest).to receive(:get_bamboo_settings).with("/rest/admin/latest/permissions/groups?name=#{resource[:name]}").and_return([{'permissions' => []}])
      provider.permissions
    end
  end
end
