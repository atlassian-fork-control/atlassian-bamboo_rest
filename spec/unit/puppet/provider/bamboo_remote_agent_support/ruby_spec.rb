require 'spec_helper'

describe Puppet::Type.type(:bamboo_remote_agent_support).provider(:ruby) do |variable|

  describe '.map_config_to_resource_hash' do
    let(:remote_agent_support) do
      {
        'remoteAgentsSupported' => true,
      }
    end

    let(:resource_hash) do
      described_class::map_config_to_resource_hash(remote_agent_support)
    end

    specify { expect(resource_hash[:remote_agents_supported]).to eq(:true) }
  end

  describe '#map_resource_to_config' do

    let(:instance) do
      instance = described_class.new()
      resource = {:remote_agents_supported => false}
      instance.resource = resource
      instance
    end

    let(:config) do
      instance.map_resource_hash_to_config
    end

    specify { expect(config['remoteAgentsSupported']).to eq(false)}
  end
end