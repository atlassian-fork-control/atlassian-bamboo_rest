require 'spec_helper'

describe Puppet::Type.type(:bamboo_mail_server).provider(:ruby) do |variable|
  describe '.map_config_to_resource_hash' do
    specify 'should map smtp mail configurations correctly' do
      bamboo_mail_config = {
        'name'                         => 'bamboo',
        'fromAddress'                  => 'bamboo@atlassian.com',
        'subjectPrefix'                => '[staging-bamboo]',
        'emailSetting'                 => 'SMTP',
        'precedenceBulkHeaderExcluded' => true,
        'smtpServer'                   => 'mail.atlassian.com',
        'smtpPort'                     => 22,
        'smtpUsername'                 => 'user',
        'smtpPassword'                 => 'password',
        'tlsEnabled'                   => true,
      }

      resource_hash = described_class.map_config_to_resource_hash(bamboo_mail_config)
      expect(resource_hash[:from]).to eq 'bamboo'
      expect(resource_hash[:from_address]).to eq 'bamboo@atlassian.com'
      expect(resource_hash[:subject_prefix]).to eq '[staging-bamboo]'
      expect(resource_hash[:email_settings]).to eq :SMTP
      expect(resource_hash[:precedence_bulk_header_excluded]).to eq :true
      expect(resource_hash[:smtp_server]).to eq 'mail.atlassian.com'
      expect(resource_hash[:smtp_port]).to eq 22
      expect(resource_hash[:smtp_username]).to eq 'user'
      expect(resource_hash[:smtp_password]).to eq 'password'
      expect(resource_hash[:tls_enabled]).to eq :true
    end

    specify 'should map JNDI mail configuration correctly' do
      bamboo_mail_config = {
          'name'                         => 'bamboo',
          'fromAddress'                  => 'bamboo@atlassian.com',
          'subjectPrefix'                => '[staging-bamboo]',
          'emailSetting'                 => 'JNDI',
          'precedenceBulkHeaderExcluded' => true,
          'jndiLocation'                 => 'bamboo_server',
      }

      resource_hash = described_class.map_config_to_resource_hash(bamboo_mail_config)
      expect(resource_hash[:from]).to eq 'bamboo'
      expect(resource_hash[:from_address]).to eq 'bamboo@atlassian.com'
      expect(resource_hash[:subject_prefix]).to eq '[staging-bamboo]'
      expect(resource_hash[:email_settings]).to eq :JNDI
      expect(resource_hash[:precedence_bulk_header_excluded]).to eq :true
      expect(resource_hash[:jndi_location]).to eq 'bamboo_server'
    end
  end

  describe '#map_resource_hash_to_config' do
    specify 'should map smtp mail configurations correctly' do
      provider = described_class.new
      resource = {
        :from                            => 'bamboo',
        :from_address                    => 'bamboo@atlassian.com',
        :subject_prefix                  => '[staging-bamboo]',
        :precedence_bulk_header_excluded => :true,
        :email_settings                  => :SMTP,
        :smtp_server                     => 'mail.atlassian.com',
        :smtp_port                       => 22,
        :smtp_username                   => 'user',
        :smtp_password                   => 'encrypted',
        :tls_enabled                     => :false
      }
      provider.resource = resource
      json = provider.map_resource_hash_to_config

      expect(json['name']).to eq 'bamboo'
      expect(json['fromAddress']).to eq 'bamboo@atlassian.com'
      expect(json['subjectPrefix']).to eq '[staging-bamboo]'
      expect(json['emailSetting']).to eq :SMTP
      expect(json['precedenceBulkHeaderExcluded']).to eq :true
      expect(json['smtpServer']).to eq 'mail.atlassian.com'
      expect(json['smtpPort']).to eq 22
      expect(json['smtpUsername']).to eq 'user'
      expect(json['smtpPassword']).to eq 'encrypted'
      expect(json['tlsEnabled']).to eq :false
    end

    specify 'should map jndi mail configuration correctly' do
      provider = described_class.new
      resource = {
        :from                            => 'bamboo',
        :from_address                    => 'bamboo@atlassian.com',
        :subject_prefix                  => '[staging-bamboo]',
        :precedence_bulk_header_excluded => :true,
        :email_settings                  => :JNDI,
        :jndi_location                   => 'mailServer',

      }

      provider.resource = resource
      json = provider.map_resource_hash_to_config

      expect(json['name']).to eq 'bamboo'
      expect(json['fromAddress']).to eq 'bamboo@atlassian.com'
      expect(json['subjectPrefix']).to eq '[staging-bamboo]'
      expect(json['emailSetting']).to eq :JNDI
      expect(json['precedenceBulkHeaderExcluded']).to eq :true
      expect(json['jndiLocation']).to eq 'mailServer'
    end

    specify 'should fail if from is missing' do
      provider = described_class.new
      resource = {
          :from_address                    => 'bamboo@atlassian.com',
          :subject_prefix                  => '[staging-bamboo]',
          :precedence_bulk_header_excluded => :true,
          :email_settings                  => :JNDI,
          :jndi_location                   => 'mailServer',

      }

      provider.resource = resource
      expect { provider.map_resource_hash_to_config }.to raise_error(ArgumentError)
    end

    specify 'should fail if from_address is missing' do
      provider = described_class.new
      resource = {
          :from                            => 'bamboo',
          :subject_prefix                  => '[staging-bamboo]',
          :precedence_bulk_header_excluded => :true,
          :email_settings                  => :JNDI,
          :jndi_location                   => 'mailServer',

      }

      provider.resource = resource
      expect { provider.map_resource_hash_to_config }.to raise_error(ArgumentError)
    end

    specify 'should fail if email_settings is missing' do
      provider = described_class.new
      resource = {
          :from                            => 'bamboo',
          :from_address                    => 'bamboo@atlassian.com',
          :subject_prefix                  => '[staging-bamboo]',
          :precedence_bulk_header_excluded => :true,
          :jndi_location                   => 'mailServer',

      }

      provider.resource = resource
      expect { provider.map_resource_hash_to_config }.to raise_error(ArgumentError)
    end
  end
end
