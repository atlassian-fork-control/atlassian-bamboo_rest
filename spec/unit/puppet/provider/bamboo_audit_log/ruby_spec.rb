require 'spec_helper'

describe Puppet::Type.type(:bamboo_audit_log).provider(:ruby) do |variable|

  describe '.map_config_to_resource_hash' do
    let(:audit_log_settings) do
      {
          'auditLoggingEnabled' => true,
      }
    end

    let(:resource_hash) do
      described_class::map_config_to_resource_hash(audit_log_settings)
    end

    specify { expect(resource_hash[:audit_log_enabled]).to eq(:true) }
  end

  describe '#map_resource_to_config' do

    let(:instance) do
      instance = described_class.new()
      resource = {:audit_log_enabled => false}
      instance.resource = resource
      instance
    end

    let(:config) do
      instance.map_resource_hash_to_config
    end

    specify { expect(config['auditLoggingEnabled']).to eq(false)}
  end
end