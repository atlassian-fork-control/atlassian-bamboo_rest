require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', '..', '..', 'lib', 'puppet', 'provider', 'bamboo_ensurable_provider.rb'))
require 'spec_helper'


type_class = Puppet::Type.newtype(:test) do

  newparam(:name, :namevar =>true)do

  end

  newproperty(:value) do

  end
end

describe Puppet::Provider::BambooEnsurableProvider do
  describe '.instances' do
    specify 'should return correct providers' do
      expect(Bamboo::Rest).to receive(:get_bamboo_settings).with('/rest/admin/latest/idonotexist/').and_return(
        {
          'bambooResources' => {
              'bambooResources' => [
                {
                  'id' => 1,
                  'name' => 'foo',
                  'value' => 'bar',
                },
            ]
          }
        }
      )

      described_class.instances
    end
  end

  describe '.prefetch' do
    specify 'should match resource and provider correctly' do
      provider = described_class.new(
          {
              :id => 1,
              :name => 'foo',
              :value => 'bar',
              :encrypted => false,
          }
      )

      expect(described_class).to receive(:instances).and_return([provider])
      resource = type_class.new({ :name => 'foo', :value => 'bar'})

      described_class.prefetch({'foo' => resource})
      expect (resource.provider) == provider
    end
  end

  describe '#create' do
    specify 'should invoke rest module with the right params' do
      provider = described_class.new
      config = {'name' => 'foo', 'value' => 'bar'}
      expect(provider).to receive(:map_resource_hash_to_config).and_return(config)
      expect(Bamboo::Rest).to receive(:create_bamboo_resource).with('/rest/admin/latest/idonotexist/', config)
      provider.create()
    end
  end

  describe '#flush' do
    specify 'should invoke rest module with the right params' do
      provider = described_class.new(
          {
              :id => 1,
              :name => 'foo',
              :value => 'bar',
              :encrypted => false,
          }
      )
      config = {'name' => 'foo', 'value' => 'bar'}
      expect(provider).to receive(:map_resource_hash_to_config).and_return(config)
      expect(Bamboo::Rest).to receive(:update_bamboo_settings).with('/rest/admin/latest/idonotexist/1', config)
      provider.dirty_flag = true
      provider.flush()
    end

    specify 'should not update bamboo if no property is changed' do
      provider = described_class.new()

      expect(Bamboo::Rest).not_to receive(:update_bamboo_settings)
      provider.flush()
    end
  end

  describe '#destroy' do
    specify 'should invoke bamboo rest endpoint with right params' do
      provider = described_class.new(
          {
              :id => 1,
              :name => 'foo',
              :value => 'bar',
              :encrypted => false,
          }
      )
      expect(Bamboo::Rest).to receive(:delete_bamboo_resource).with('/rest/admin/latest/idonotexist/1')
      provider.destroy()
    end
  end
end
