require 'spec_helper'

describe Puppet::Type.type(:bamboo_user_repositories).provider(:ruby) do |variable|
  describe '.map_config_to_resource_hash' do
    let (:bamboo_user_repositories) do
      {
        'type'                 => "Crowd",
        'serverURL'            => "http://localhost:8095/crowd",
        'applicationName'      => "bamboo",
        'applicationPassword'  => "test",
        'cacheRefreshInterval' => 60
      }
    end

    let(:resource_hash) do
      described_class::map_config_to_resource_hash(bamboo_user_repositories)
    end

    specify { expect(resource_hash[:type]).to eq "Crowd" }
    specify { expect(resource_hash[:server_url]).to eq "http://localhost:8095/crowd"}
    specify { expect(resource_hash[:application_name]).to eq "bamboo"}
    specify { expect(resource_hash[:application_password]).to eq "test"}
    specify { expect(resource_hash[:cache_refresh_interval]).to eq 60}

  end

  describe '#map_resource_to_config' do
    let(:instance) do
      instance = described_class.new()
      resource = {
        :type                   => "Crowd",
        :server_url             => "http://localhost:6990/bamboo",
        :application_name       => "bamboo",
        :application_password   => "test",
        :cache_refresh_interval => 60,
      }
      instance.resource = resource
      instance
    end

    let(:bamboo_user_repositories) do
      instance.map_resource_hash_to_config
    end

    specify { expect(bamboo_user_repositories['type']).to eq "Crowd" }
    specify { expect(bamboo_user_repositories['serverURL']).to eq "http://localhost:6990/bamboo" }
    specify { expect(bamboo_user_repositories['applicationName']).to eq "bamboo" }
    specify { expect(bamboo_user_repositories['applicationPassword']).to eq "test" }
    specify { expect(bamboo_user_repositories['cacheRefreshInterval']).to eq 60 }
  end

end
