require 'spec_helper'
require File.expand_path(File.join(File.dirname(__FILE__), '../../../../../', 'lib/puppet/provider/bamboo_artifact_handler/bamboo_artifact_handler_provider.rb'))

describe Puppet::Provider::BambooArtifactHandlerProvider do
  describe '.map_config_to_resource_hash' do
    specify 'should convert json to resource_hash correctly' do
      json = {'sharedArtifactsEnabled' => 'true', 'nonsharedArtifactsEnabled' => 'false'}
      resource_hash = described_class.map_config_to_resource_hash(json)
      expect(resource_hash[:shared_artifacts_enabled]).to eq(:true)
      expect(resource_hash[:non_shared_artifacts_enabled]).to eq(:false)
    end
  end

  describe '#map_resource_hash_to_config' do
    specify 'should convert resource_hash to json correctly' do
      resource_hash = {:shared_artifacts_enabled => :true, :non_shared_artifacts_enabled => :false}
      provider = described_class.new
      provider.resource = resource_hash
      json = provider.map_resource_hash_to_config
      expect(json['sharedArtifactsEnabled']).to eq('true')
      expect(json['nonsharedArtifactsEnabled']).to eq('false')
    end

    specify 'should convert partial resource hash to json correctly' do
      resource_hash = {:shared_artifacts_enabled => :true}
      provider = described_class.new
      provider.resource = resource_hash
      json = provider.map_resource_hash_to_config
      expect(json['sharedArtifactsEnabled']).to eq('true')
      expect(json['nonsharedArtifactsEnabled'].nil?).to eq(true)
    end
  end
end