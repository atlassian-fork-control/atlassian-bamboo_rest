require 'spec_helper'

describe Puppet::Type.type(:bamboo_im_server).provider(:ruby) do |variable|
  describe '.map_config_to_resource_hash' do
    specify 'should map 404 response to empty array' do
      bamboo_response = {
        'status-code' => 404,
      }

      resource_hash = described_class.map_config_to_resource_hash(bamboo_response)
      expect(resource_hash.empty?).to equal(true)
    end

    specify 'should map bamboo im server config correctly' do
      bamboo_response = {
        'host'                    => 'mail.a.c',
        'port'                    => 22,
        'username'                => 'foo',
        'password'                => 'bar',
        'resource'                => 'bamboo',
        'requireTLSSSLConnection' => true,
      }

      resource_hash = described_class.map_config_to_resource_hash(bamboo_response)[0]
      expect(resource_hash.host).to eq('mail.a.c')
      expect(resource_hash.port).to eq(22)
      expect(resource_hash.username).to eq('foo')
      expect(resource_hash.password).to eq('bar')
      expect(resource_hash.resource_name).to eq('bamboo')
      expect(resource_hash.tls_enabled).to eq(:true)
    end
  end

  describe '#map_resource_hash_to_config' do
    specify 'given valid resource hash with only host defined' do
      provider = described_class.new
      resource = {:host => 'mail.a.c'}
      provider.resource = resource
      config = provider.map_resource_hash_to_config
      expect(config['host']).to eq 'mail.a.c'
      expect(config['port']).to eq nil
      expect(config['username']).to eq nil
      expect(config['password']).to eq nil
      expect(config['resource']).to eq nil
      expect(config['requireTLSSSLConnection']).to eq nil
    end

    specify 'given valid resource hash' do
        provider = described_class.new
        resource = {:host => 'mail.a.c', :port => 22, :username => 'foo', :password => 'bar', :resource_name => 'bamboo', :tls_enabled => :false}
        provider.resource = resource
        config = provider.map_resource_hash_to_config
        expect(config['host']).to eq 'mail.a.c'
        expect(config['port']).to eq 22
        expect(config['username']).to eq 'foo'
        expect(config['password']).to eq 'bar'
        expect(config['resource']).to eq 'bamboo'
        expect(config['requireTLSSSLConnection']).to eq :false
    end
  end

  describe '#destroy' do
    specify 'correct REST endpoint should be invoked' do
      provider = described_class.new
      expect(Bamboo::Rest).to receive(:delete_bamboo_resource).with(described_class.bamboo_resource)
      provider.destroy
    end
  end

  describe '#flush' do
    specify 'correct REST endpoint should be invoked' do
      provider = described_class.new
      config = {'host' => 'mail.a.c'}
      expect(provider).to receive(:map_resource_hash_to_config).and_return(config)
      provider.dirty_flag = true
      expect(Bamboo::Rest).to receive(:update_bamboo_settings).with(described_class.bamboo_resource, config)
      provider.flush
    end
  end

end
