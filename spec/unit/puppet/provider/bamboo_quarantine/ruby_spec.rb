require 'spec_helper'

describe Puppet::Type.type(:bamboo_quarantine).provider(:ruby) do |variable|

  describe '.map_config_to_resource_hash' do
    let(:quarantine_settings) do
      {
        'quarantineTestsEnabled' => true,
      }
    end

    let(:resource_hash) do
      described_class::map_config_to_resource_hash(quarantine_settings)
    end

    specify { expect(resource_hash[:quarantine_tests_enabled]).to eq(:true) }
  end

  describe '#map_resource_to_config' do

    let(:instance) do
      instance = described_class.new()
      resource = {:quarantine_tests_enabled => false}
      instance.resource = resource
      instance
    end

    let(:config) do
      instance.map_resource_hash_to_config
    end

    specify { expect(config['quarantineTestsEnabled']).to eq(false)}
  end
end