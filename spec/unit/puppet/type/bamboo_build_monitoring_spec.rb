require 'spec_helper'

describe Puppet::Type.type(:bamboo_build_monitoring) do

  describe '#name' do
    it 'should only accept value current' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true')}.to_not raise_error
      expect { described_class.new(:name => 'not_current', :build_monitoring_enabled => 'true')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'build_monitoring_enabled' do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true')}.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'false')}.to_not raise_error
    end

    specify 'should fail if passed a non-boolean variable' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'abcd')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'build_queue_minutes_timeout_default' do
    specify 'should accept positive integer' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :build_queue_minutes_timeout_default => 3)}.to_not raise_error
    end

    specify 'should not accept zero' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :build_queue_minutes_timeout_default => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept negative number' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :build_queue_minutes_timeout_default => -1)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept non-digital values' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :build_queue_minutes_timeout_default => 'abc')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'log_quiet_minutes_time_default' do
    specify 'should accept positive integer' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :log_quiet_minutes_time_default => 3)}.to_not raise_error
    end

    specify 'should not accept zero' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :log_quiet_minutes_time_default => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept negative number' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :log_quiet_minutes_time_default => -1)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept non-digital values' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :log_quiet_minutes_time_default => 'abc')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'build_time_multiplier_default' do
    specify 'should accept positive integers' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :build_time_multiplier_default => 3)}.to_not raise_error
    end

    specify 'should accept positive decimals' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :build_time_multiplier_default => 5.0)}.to_not raise_error
    end

    specify 'should not accept zero' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :build_time_multiplier_default => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept negative integer' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :build_time_multiplier_default => -1)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept negative number' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :build_time_multiplier_default => -1.5)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept non-digital values' do
      expect { described_class.new(:name => 'current', :build_monitoring_enabled => 'true', :build_time_multiplier_default => 'abc')}.to raise_error(Puppet::ResourceError)
    end
  end
end
