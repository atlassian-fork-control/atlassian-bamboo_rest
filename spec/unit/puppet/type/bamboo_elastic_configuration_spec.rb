require 'spec_helper'


describe Puppet::Type.type(:bamboo_elastic_configuration) do
  describe '#name' do
    it 'should only accept value current' do
      expect { described_class.new(:name => 'current', :enabled => true) }.to_not raise_error
      expect { described_class.new(:name => 'not_current', :enabled => true) }.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'enabled' do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :enabled => true) }.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :enabled => false) }.to_not raise_error
    end

    specify 'should not accept non boolean values' do
      expect { described_class.new(:name => 'current', :enabled => 'Blabla') }.to raise_error(Puppet::ResourceError)
    end

  end

  describe 'access_key' do
    specify 'should accept string' do
      expect { described_class.new(:name => 'current', :access_key => 'ABC') }.to_not raise_error
    end

    specify 'should not accept empty string' do
      expect { described_class.new(:name => 'current', :access_key => '') }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject false' do
      expect { described_class.new(:name => 'current', :access_key => false) }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject nil' do
      expect { described_class.new(:name => 'current', :access_key => nil) }.to raise_error(Puppet::Error)
    end
  end

  describe 'secret_key' do
    specify 'should accept string' do
      expect { described_class.new(:name => 'current', :secret_key => '123') }.to_not raise_error
    end

    specify 'should not accept empty string' do
      expect { described_class.new(:name => 'current', :secret_key => '') }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject false' do
      expect { described_class.new(:name => 'current', :secret_key => false) }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject nil' do
      expect { described_class.new(:name => 'current', :secret_key => nil) }.to raise_error(Puppet::Error)
    end

    specify 'should not log actual value change' do
      expect( described_class.new(:name => 'current', :secret_key => 'bar').property(:secret_key).change_to_s('bar', 'foobar') ).to eq('The secret_key variable has been changed')
    end
  end

  describe 'region' do
    specify 'should accept valid region' do
      expect { described_class.new(:name => 'current', :region => 'US_EAST_1') }.to_not raise_error
    end

    specify 'should not accept random string' do
      expect { described_class.new(:name => 'current', :region => '123') }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject false' do
      expect { described_class.new(:name => 'current', :region => false) }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject nil' do
      expect { described_class.new(:name => 'current', :region => nil) }.to raise_error(Puppet::Error)
    end
  end

  describe 'private_key_file' do
    specify 'should accept string' do
      expect { described_class.new(:name => 'current', :private_key_file => '123') }.to_not raise_error
    end

    specify 'should not accept empty string' do
      expect { described_class.new(:name => 'current', :private_key_file => '') }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject false' do
      expect { described_class.new(:name => 'current', :private_key_file => false) }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject nil' do
      expect { described_class.new(:name => 'current', :private_key_file => nil) }.to raise_error(Puppet::Error)
    end
  end

  describe 'certificate_file' do
    specify 'should accept string' do
      expect { described_class.new(:name => 'current', :certificate_file => '123') }.to_not raise_error
    end

    specify 'should not accept empty string' do
      expect { described_class.new(:name => 'current', :certificate_file => '') }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject false' do
      expect { described_class.new(:name => 'current', :certificate_file => false) }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject nil' do
      expect { described_class.new(:name => 'current', :certificate_file => nil) }.to raise_error(Puppet::Error)
    end
  end

  describe 'upload_aws_identifier' do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :upload_aws_identifier => true) }.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :upload_aws_identifier => false) }.to_not raise_error
    end

    specify 'should not accept non boolean values' do
      expect { described_class.new(:name => 'current', :upload_aws_identifier => 'Blabla') }.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'max_elastic_instances' do
    specify 'should not accept negative integer' do
      expect { described_class.new(:name => 'current', :max_elastic_instances => -3)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept 0' do
      expect { described_class.new(:name => 'current', :max_elastic_instances => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should accept positive number' do
      expect { described_class.new(:name => 'current', :max_elastic_instances => 4)}.to_not raise_error
    end

    specify 'should not accept string' do
      expect { described_class.new(:name => 'current', :max_elastic_instances => "Stringy")}.to raise_error(Puppet::ResourceError)
    end

  end

  describe 'allocate_public_ip_to_vpc_instances' do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :allocate_public_ip_to_vpc_instances => true) }.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :allocate_public_ip_to_vpc_instances => false) }.to_not raise_error
    end

    specify 'should not accept non boolean values' do
      expect { described_class.new(:name => 'current', :allocate_public_ip_to_vpc_instances => 'Blabla') }.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'im_type' do
    specify 'should accept given management plans' do
      im_types = ["Disabled", "Custom", "Passive", "Aggressive", "Default"]
      im_types.each do |type|
        expect { described_class.new(:name => 'current', :im_type => type)}.to_not raise_error
      end
    end

    specify 'should not accept any other val' do
      expect { described_class.new(:name => 'current', :im_type => 'Agro')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'im_idle_shutdown_delay_minutes' do
    specify 'should reject negative shutdown delay' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_idle_shutdown_delay_minutes => -3)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject shutdown delay = 0' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_idle_shutdown_delay_minutes => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject shutdown delay with non-custom' do
      expect { described_class.new(:name => 'current', :im_type => 'Agressive', :im_idle_shutdown_delay_minutes => 5)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject string shutdown delay' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_idle_shutdown_delay_minutes => "Stringy")}.to raise_error(Puppet::ResourceError)
    end

    specify 'should accept valid shutdown delay' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_idle_shutdown_delay_minutes => 10)}.to_not raise_error
    end
  end

  describe 'im_allowed_non_bamboo_instances' do
    specify 'should reject negative allowed non bamboo instances' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_allowed_non_bamboo_instances => -3)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject allowed non bamboo instances = 0' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_allowed_non_bamboo_instances => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject allowed non bamboo instances with non-custom' do
      expect { described_class.new(:name => 'current', :im_type => 'Agressive', :im_allowed_non_bamboo_instances => 5)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject string allowed non bamboo instances' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_allowed_non_bamboo_instances => "Stringy")}.to raise_error(Puppet::ResourceError)
    end

    specify 'should accept valid allowed non bamboo instances' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_allowed_non_bamboo_instances => 10)}.to_not raise_error
    end
  end

  describe 'im_max_num_to_start' do
    specify 'should reject negative max num to start' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_max_num_to_start => -3)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject max num to start = 0' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_max_num_to_start => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject max num to start with non-custom' do
      expect { described_class.new(:name => 'current', :im_type => 'Agressive', :im_max_num_to_start => 5)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject string max num to start' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_max_num_to_start => "Stringy")}.to raise_error(Puppet::ResourceError)
    end

    specify 'should accept valid max num to start' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_max_num_to_start => 10)}.to_not raise_error
    end

  end

  describe 'im_queued_build_threshold' do
    specify 'should reject negative queued build threshold' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_queued_build_threshold => -3)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject queued build threshold = 0' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_queued_build_threshold => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject queued build threshold with non-custom' do
      expect { described_class.new(:name => 'current', :im_type => 'Agressive', :im_queued_build_threshold => 5)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject string queued build threshold' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_queued_build_threshold => "Stringy")}.to raise_error(Puppet::ResourceError)
    end

    specify 'should accept valid queued build threshold' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_queued_build_threshold => 10)}.to_not raise_error
    end
  end

  describe 'im_elastic_queued_build_threshold' do
    specify 'should reject negative elastic queued build threshold' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_elastic_queued_build_threshold => -3)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject elastic queued build threshold = 0' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_elastic_queued_build_threshold => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject elastic queued build threshold with non-custom' do
      expect { described_class.new(:name => 'current', :im_type => 'Agressive', :im_elastic_queued_build_threshold => 5)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject string elastic queued build threshold' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_elastic_queued_build_threshold => "Stringy")}.to raise_error(Puppet::ResourceError)
    end

    specify 'should accept valid elastic queued build threshold' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_elastic_queued_build_threshold => 10)}.to_not raise_error
    end

    specify 'should reject if elastic queued build threshold > queued build threshold' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_elastic_queued_build_threshold => 10, :im_queued_build_threshold => 9)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should accept if elastic queued build threshold = queued build threshold' do
     expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_elastic_queued_build_threshold => 10, :im_queued_build_threshold => 10)}.to_not raise_error
    end

    specify 'should accept if elastic queued build threshold < queued build threshold' do
     expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_elastic_queued_build_threshold => 10, :im_queued_build_threshold => 11)}.to_not raise_error
    end
  end

  describe 'im_average_queue_time' do
    specify 'should reject negative average queue time' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_average_queue_time => -3)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject average queue time = 0' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_average_queue_time => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject average queue time with non-custom' do
      expect { described_class.new(:name => 'current', :im_type => 'Agressive', :im_average_queue_time => 5)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject string average queue time' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_average_queue_time => "Stringy")}.to raise_error(Puppet::ResourceError)
    end

    specify 'should accept valid average queue time' do
      expect { described_class.new(:name => 'current', :im_type => 'Custom', :im_average_queue_time => 10)}.to_not raise_error
    end
  end


  describe 'termination_enabled' do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :termination_enabled => true) }.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :termination_enabled => false) }.to_not raise_error
    end

    specify 'should not accept non boolean values' do
      expect { described_class.new(:name => 'current', :termination_enabled => 'Blabla') }.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'termination_shutdown_delay' do
    specify 'should reject string termination shutdown delay' do
        expect { described_class.new(:name => 'current', :termination_enabled => true, :termination_shutdown_delay => "blabla") }.to raise_error(Puppet::ResourceError)
    end

    specify 'should reject negative termination shutdown delay' do
        expect { described_class.new(:name => 'current', :termination_enabled => true, :termination_shutdown_delay => -7) }.to raise_error(Puppet::ResourceError)
    end

    specify 'should accept 0 for termination shutdown delay' do
        expect { described_class.new(:name => 'current', :termination_enabled => true, :termination_shutdown_delay => 0) }.to_not raise_error
    end

    specify 'should accept positive termination shutdown delay' do
        expect { described_class.new(:name => 'current', :termination_enabled => true, :termination_shutdown_delay => 300) }.to_not raise_error
    end

    specify 'should not accept if termination enabled is false' do
        expect { described_class.new(:name => 'current', :termination_enabled => false, :termination_shutdown_delay => 300) }.to raise_error(Puppet::ResourceError)
    end

  end

end
