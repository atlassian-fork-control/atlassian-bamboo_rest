require 'spec_helper'

describe Puppet::Type.type(:bamboo_build_expiry) do

  describe '#name' do
    it 'should only accept current' do
      expect { described_class.new(:name => 'current', :duration => 3)}.to_not raise_error
      expect { described_class.new(:name => 'not _current', :duration => 3)}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'duration' do
    it 'should only be a non-negative integer' do
      expect { described_class.new(:name => 'current', :duration => 3)}.to_not raise_error
      expect { described_class.new(:name => 'current', :duration => 0)}.to_not raise_error
      expect { described_class.new(:name => 'current', :duration => -1)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :duration => 'sdfsdf')}.to raise_error(Puppet::ResourceError)
    end

    specify 'both duration and builds to keep can not be 0' do
      expect { described_class.new(:name => 'current', :duration => 0, :builds_to_keep => 0)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :duration => 0, :builds_to_keep => 1)}.to_not raise_error
      expect { described_class.new(:name => 'current', :duration => 1, :builds_to_keep => 0)}.to_not raise_error
      expect { described_class.new(:name => 'current', :duration => 1, :builds_to_keep => 1)}.to_not raise_error
    end
  end

  describe 'period' do
    specify 'duration should only be days, weeks, or months' do
      expect { described_class.new(:name => 'current', :period => 'days')}.to_not raise_error
      expect { described_class.new(:name => 'current', :period => 'weeks')}.to_not raise_error
      expect { described_class.new(:name => 'current', :period => 'months')}.to_not raise_error
      expect { described_class.new(:name => 'current', :period => 'day')}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :period => 'sfwercrwf')}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :period => :false)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :period => 23)}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'builds_to_keep' do
    it 'should only be a non negative integer' do
      expect { described_class.new(:name => 'current', :builds_to_keep => 3)}.to_not raise_error
      expect { described_class.new(:name => 'current', :builds_to_keep => 0)}.to_not raise_error
      expect { described_class.new(:name => 'current', :builds_to_keep => -1)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :builds_to_keep => 'sdfsdf')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'deployments_to_keep' do
    it 'should be an integer >= 2' do
      expect { described_class.new(:name => 'current', :deployments_to_keep => 10)}.to_not raise_error
      expect { described_class.new(:name => 'current', :deployments_to_keep => 2)}.to_not raise_error
      expect { described_class.new(:name => 'current', :deployments_to_keep => 1)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :deployments_to_keep => 0)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :deployments_to_keep => -1)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :deployments_to_keep => 'sdfsdf')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'maximum_ignored_log_size' do
    it 'can be any integer' do
      expect { described_class.new(:name => 'current', :maximum_ignored_log_size => 10)}.to_not raise_error
      expect { described_class.new(:name => 'current', :maximum_ignored_log_size => 0)}.to_not raise_error
      expect { described_class.new(:name => 'current', :maximum_ignored_log_size => -1)}.to_not raise_error
      expect { described_class.new(:name => 'current', :maximum_ignored_log_size => "sfewf4")}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'expire_results' do
    it 'can only be a boolean' do
      expect { described_class.new(:name => 'current', :expire_results => :false)}.to_not raise_error
      expect { described_class.new(:name => 'current', :expire_results => :true)}.to_not raise_error
      expect { described_class.new(:name => 'current', :expire_results => 10)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :expire_results => :random_symbol)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :expire_results => "stringy")}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'expire_artifacts' do
    it 'can only be a boolean' do
      expect { described_class.new(:name => 'current', :expire_artifacts => :false)}.to_not raise_error
      expect { described_class.new(:name => 'current', :expire_artifacts => :true)}.to_not raise_error
      expect { described_class.new(:name => 'current', :expire_artifacts => 10)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :expire_artifacts => :random_symbol)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :expire_artifacts => "stringy")}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'expire_logs' do
    it 'can only be a boolean' do
      expect { described_class.new(:name => 'current', :expire_logs => :false)}.to_not raise_error
      expect { described_class.new(:name => 'current', :expire_logs => :true)}.to_not raise_error
      expect { described_class.new(:name => 'current', :expire_logs => 10)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :expire_logs => :random_symbol)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :expire_logs => "stringy")}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'labels_to_exclude' do
    it 'can only be a string' do
      expect { described_class.new(:name => 'current', :labels_to_exclude => "stringy")}.to_not raise_error
      expect { described_class.new(:name => 'current', :labels_to_exclude => :true)}.to raise_error(Puppet::ResourceError)
      expect { described_class.new(:name => 'current', :labels_to_exclude => 123)}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'cron_expression' do
    it 'must be a valid cron expression' do
      expect { described_class.new(:name => 'current', :cron_expression => "0 0 0 ? * *")}.to_not raise_error
    end
  end

end
