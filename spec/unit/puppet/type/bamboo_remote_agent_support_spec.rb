require 'spec_helper'

describe Puppet::Type.type(:bamboo_remote_agent_support) do
  describe '#name' do
    it 'should only accept value current' do
      expect {described_class.new(:name => 'current', :remote_agents_supported => :true)}.to_not raise_error
      expect {described_class.new(:name => 'not_current', :remote_agents_supported => :true)}.to raise_error(Puppet::ResourceError)
    end
  end

  describe '#remote_agents_supported' do
    specify 'should accept true' do
      expect {described_class.new(:name => 'current', :remote_agents_supported => :true)}.to_not raise_error
    end

    specify 'should accept false' do
      expect {described_class.new(:name => 'current', :remote_agents_supported => :false)}.to_not raise_error
    end

    specify 'should not accept non boolean values' do
      expect {described_class.new(:name => 'current', :remote_agents_supported => 'test')}.to raise_error(Puppet::ResourceError)
    end
  end
end
