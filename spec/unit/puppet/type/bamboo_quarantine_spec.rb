require 'spec_helper'

describe Puppet::Type.type(:bamboo_quarantine) do
  describe '#name' do
    it 'should only accept value current' do
      expect {described_class.new(:name => 'current', :quarantine_tests_enabled => :true)}.to_not raise_error
      expect {described_class.new(:name => 'not_current', :quarantine_tests_enabled => :true)}.to raise_error(Puppet::ResourceError)
    end
  end

  describe '#quarantine_tests_enabled' do
    specify 'should accept true' do
      expect {described_class.new(:name => 'current', :quarantine_tests_enabled => :true)}.to_not raise_error
    end

    specify 'should accept false' do
      expect {described_class.new(:name => 'current', :quarantine_tests_enabled => :false)}.to_not raise_error
    end

    specify 'should not accept non boolean values' do
      expect {described_class.new(:name => 'current', :quarantine_tests_enabled => 'test')}.to raise_error(Puppet::ResourceError)
    end
  end
end
