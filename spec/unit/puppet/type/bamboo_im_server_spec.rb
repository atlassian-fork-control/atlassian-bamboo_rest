require 'spec_helper'

describe Puppet::Type.type(:bamboo_im_server) do
  describe '#name' do
    specify 'should accept value current' do
      expect {described_class.new(:name => 'current', :host => 'hipchat.com')}.to_not raise_error
    end

    specify 'should not accept values other than current' do
      expect {described_class.new(:name => 'test', :host => 'hipchat.com')}.to raise_error(Puppet::ResourceError)
    end

  end

  describe '#host' do
    specify 'should not be empty' do
      expect {described_class.new(:name => 'current', :host => '')}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not be nil' do
      expect {described_class.new(:name => 'current', :ensure => 'present')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe '#port' do
    specify 'should accept positive integer' do
      expect {described_class.new(:name => 'current', :host => 'hipchat.com', :port => 22)}.to_not raise_error
    end

    specify 'should not accept negative integer' do
      expect {described_class.new(:name => 'current', :host => 'hipchat.com', :port => -22)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept negative alphabetic letters' do
      expect {described_class.new(:name => 'current', :host => 'hipchat.com', :port => 'abc22')}.to raise_error(Puppet::ResourceError)
    end
  end
end
