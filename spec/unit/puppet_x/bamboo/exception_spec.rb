require 'puppet_x/bamboo/exception'
require 'spec_helper'

describe Bamboo::ExceptionHandler do
  describe 'retrieve_error_message' do
  	specify { expect(described_class.retrieve_error_message('')).to eq('unknown')}
  end

  describe 'retrieve_error_message' do
  	specify { expect(described_class.retrieve_error_message('<html><p>error</p></html>')).to eq('error')}
  end

  describe 'retrieve_error_message' do
  	specify { expect(described_class.retrieve_error_message('<html>error</html>')).to eq('<html>error</html>')}
  end

  describe 'retrieve_error_message' do
  	specify { expect(described_class.retrieve_error_message('Failed to retrieve bamboo configuration')).to eq('Failed to retrieve bamboo configuration')}
  end

  describe 'retrieve_error_message' do
  	specify { expect(described_class.retrieve_error_message('{"warnings":["need to restart bamboo"]}')).to eq('{"warnings":["need to restart bamboo"]}')}
  end

  describe 'retrieve_error_message' do
  	specify { expect(described_class.retrieve_error_message('{"fieldErrors":{"field1": ["error1","error2"]}}')).to eq("\nfield1: error1 error2")}
  end

  describe 'retrieve_error_message' do
    specify { expect(described_class.retrieve_error_message('{"errors": ["error1","error2"]}')).to eq("\nerror1 error2")}
  end

  describe 'retrieve_error_message' do
    specify { expect(described_class.retrieve_error_message('{"errors": ["error1","error2"], "fieldErrors":{"field1": ["error3","error4"]}}')).to eq("\nerror1 error2\nfield1: error3 error4")}
  end
end
