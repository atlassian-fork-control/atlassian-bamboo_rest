require 'puppet_x/bamboo/config'
require 'puppet_x/bamboo/exception'
require 'puppet_x/bamboo/rest'
require 'spec_helper'
include WebMock::API

describe Bamboo::Rest do
  before(:each) do
    allow(Bamboo::Config).to receive(:read_config).and_return({
      :bamboo_base_url   => 'http://example.com',
      :admin_username    => 'foobar',
      :admin_password    => 'secret',
      :bamboo_home_dir   => '/opt/config',
    })
  end

  before(:each) do
    # health check always successful ...
    service = double('Dummy service').as_null_object
    allow(Bamboo::Rest).to receive(:init_service).and_return(service)
  end

  after(:each) do
    Bamboo::Config.reset
  end

  describe 'get_bamboo_settings' do
    specify 'should parse JSON response' do
      stub_request(:any, /.*/).to_return(:body =>
        '{
          "instanceName": "Atlassian Bamboo",
          "baseUrl": "http://ttsang:6990/bamboo"
        }')
      instances = Bamboo::Rest.get_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration')
      expect(instances.size).to eq(2)
    end

    specify 'should accept application/json' do
      stub = stub_request(:get, /.*/).with(:headers => {'Accept' => 'application/json'}).to_return(:body => '{}')
      allow(described_class).to receive(:get_user_repository_type).and_return(:hibernate)
      Bamboo::Rest.get_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration')
      expect(stub).to have_been_requested
    end

    specify 'should send credentials' do
      stub = stub_request(:get, /.*/).with(basic_auth: ['foobar', 'secret']).to_return(:body => '{}')
      Bamboo::Rest.get_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration')
      expect(stub).to have_been_requested
    end

    specify 'should raise an error if response is not expected' do
      stub_request(:any, /.*/).to_return(:status => 503)
      expect { Bamboo::Rest.get_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration') }.to raise_error(RuntimeError, /Could not get/)
    end

    specify 'should raise an error if response is not parsable' do
      stub_request(:any, /.*/).to_return(:body => 'some non-json crap')
      expect { Bamboo::Rest.get_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration') }.to raise_error(RuntimeError, /Could not parse the JSON/)
    end

    specify 'should accept closure when error happens' do
      stub_request(:any, /.*/).to_return(:status => 503)
      error = {}
      expect(error).to receive(:respond_to?).with(:http_code)
      expect {Bamboo::Rest.get_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration'){|value| error.respond_to?(:http_code)} }.to raise_error(RuntimeError, /Could not get/)
    end
  end


  describe 'update_bamboo_settings' do
    specify 'should submit a PUT to /rest/adminConfiguration/1.0/generalConfiguration' do
      stub = stub_request(:put, /rest\/adminConfiguration\/1.0\/generalConfiguration/).to_return(:status => 200)
      Bamboo::Rest.update_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration', {})
      expect(stub).to have_been_requested
    end

    specify 'should submit a POST to /rest/adminConfiguration/1.0/generalConfiguration if specified in params' do
      stub = stub_request(:post, /rest\/adminConfiguration\/1.0\/generalConfiguration/).to_return(:status => 200)
      Bamboo::Rest.update_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration', {}, 'post')
      expect(stub).to have_been_requested
    end

    specify 'should raise an error if response is not expected' do
      stub_request(:any, /.*/).to_return(:status => 503)
      expect { Bamboo::Rest.update_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration', {}) }.to raise_error(RuntimeError, /Could not update/)
    end

    specify 'should accept application/json' do
      stub = stub_request(:any, /.*/).with(:headers => {'Accept' => 'application/json'}).to_return(:body => '{}')
      Bamboo::Rest.update_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration', {})
      expect(stub).to have_been_requested
    end

    specify 'should extract error message' do
      stub = stub_request(:any, /.*/).to_return(:status => 400, :body => {'fieldErrors' => {'field1' => [{'id' => '*', 'msg' => 'Error message'}]}}.to_json)
      expect { Bamboo::Rest.update_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration', {}) }.to raise_error(RuntimeError, /Error message/)
    end

    specify 'anything besides put and post will raise an error' do
      expect {Bamboo::Rest.update_bamboo_settings('/rest/adminConfiguration/1.0/generalConfiguration', {}, :invalid_type)}.to raise_error(RuntimeError, /Invalid method given/)
    end
  end

  describe 'create_bamboo_resource' do
    specify 'should submit a POST to /rest/admin/latest/globalVariables' do
      stub = stub_request(:post, /rest\/admin\/latest\/globalVariables/).to_return(:status => 201)
      Bamboo::Rest.create_bamboo_resource('/rest/admin/latest/globalVariables', {})
      expect(stub).to have_been_requested
    end

    specify 'should use content type application/json' do
      stub = stub_request(:post, /.*/).with(:headers => {'Content-Type' => 'application/json'}, :body => {}).to_return(:status => 200)
      Bamboo::Rest.create_bamboo_resource('/rest/admin/latest/globalVariables', {})
      expect(stub).to have_been_requested
    end

    specify 'credential should be passed' do
      stub = stub_request(:post, /.*/).with(basic_auth: ['foobar', 'secret']).to_return(:status => 200)
      Bamboo::Rest.create_bamboo_resource('/rest/admin/latest/globalVariables', {})
      expect(stub).to have_been_requested
    end

    specify 'should raise an error if response is not expected' do
      stub = stub_request(:post, /.*/).to_return(:status => 500)
      expect { Bamboo::Rest.create_bamboo_resource('/rest/amdin/latest/globalVariables', {})}.to raise_error(RuntimeError, /Could not create/)
    end

    specify 'should accept application/json' do
      stub = stub_request(:post, /.*/).with(:headers => {'Accept' => 'application/json'}).to_return(:body => {}.to_json)
      Bamboo::Rest.create_bamboo_resource('/rest/admin/latest/globalVariables', {})
      expect(stub).to have_been_requested
    end

    specify 'should extract error message' do
      stub = stub_request(:post, /.*/).to_return(:status => 400, :body => {'fieldErrors' => {'field1' => [{'id' => '*', 'msg' =>  'Error message'}]}}.to_json)
      expect { Bamboo::Rest.create_bamboo_resource('/rest/admin/latest/generalVariables', {}) }.to raise_error(RuntimeError, /Error message/)
    end
  end

  describe 'delete_bamboo_resource' do
    specify 'should submit a DELETE to /rest/admin/latest/globalVariables' do
      stub = stub_request(:delete, /rest\/admin\/latest\/globalVariables\/foobar/).to_return(:status => 204)
      Bamboo::Rest.delete_bamboo_resource('/rest/admin/latest/globalVariables/foobar')
      expect(stub).to have_been_requested
    end

    specify 'should raise an error if response is not expected' do
      stub = stub_request(:delete, /.*/).to_return(:status => 500)
      expect { Bamboo::Rest.delete_bamboo_resource('/rest/amdin/latest/globalVariables/foo')}.to raise_error(RuntimeError, /Could not delete/)
    end

    specify 'should accept 404 status code' do
      stub = stub_request(:delete, /rest\/admin\/latest\/globalVariables\/foo/).to_return(:status => 404)
      Bamboo::Rest.delete_bamboo_resource('/rest/admin/latest/globalVariables/foo')
      expect(stub).to have_been_requested
    end

    specify 'should extract error message' do
      stub = stub_request(:delete, /.*/).to_return(:status => 400, :body => {'fieldErrors' => {'field1' => [{'id' => '*', 'msg' =>  'Error message'}]}}.to_json)
      expect { Bamboo::Rest.delete_bamboo_resource('/rest/admin/latest/globalVariables/foo') }.to raise_error(RuntimeError, /Error message/)
    end
  end

end
