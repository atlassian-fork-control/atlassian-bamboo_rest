require 'json'

module Bamboo
  # Exception handler to retrieve detailed error messages from http error response
  # Copied from the nexus puppet module
  class ExceptionHandler
    def self.process(e)
      # by default, include exception message
      msg = e.to_s
      msg += ', details: ' + retrieve_error_message(e.body) if e.respond_to? :body
      yield msg
    end

    def self.retrieve_error_message(data)
      if data.nil? || data.empty?
        return 'unknown'
      # even through we only accept JSON, the REST resource sometimes returns HTML
      elsif (data.is_a? String) && data.include?('<html>')
        error_message = data.match(/<p>(.*)<\/p>/)
        return error_message ? error_message[1]: data
      end

      json = data
      if data.is_a? String
        begin
          json = JSON.parse(data)
        rescue
          return data
        end
      end
      # Bamboo REST endpoint returns error messages like following format
      # {
      #    "errors" :[<error>],
      #    "fieldErrors": {
      #        field1 : [<error message>, <error message>, ..., <error message>],
      #        field2 : [<error message>, <error message>, ..., <error message>],
      #        ...
      #        fieldn : [<error message>, <error message>, ..., <error message>]
      #    }
      # }
      errors = ""
      if json['errors']
        errors += "\n" + json['errors'].join(' ')
      end
      if json['fieldErrors']
        json['fieldErrors'].each do |key, array|
            errors += "\n" + key + ': ' + array.collect{|entry| entry }.join(' ')
        end
      end
      if !errors.empty?
        errors
      else
        data
      end
    end
  end
end
