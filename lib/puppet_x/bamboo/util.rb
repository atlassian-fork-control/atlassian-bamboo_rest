module Util

  # validate resource name can only be current for some puppet type
  def self.assert_equal_to_current(value)
    raise ArgumentError, "the only acceptable name is 'current, but #{value} is provided" unless !value.nil? && value.intern == :current
  end

end
