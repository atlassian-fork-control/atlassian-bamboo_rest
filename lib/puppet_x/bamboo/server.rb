require 'json'

require File.expand_path(File.join(File.dirname(__FILE__),  'rest.rb'))

module Bamboo

  # Ensures the referenced Bamboo instance is up and running. It does the health check only once and caches the result.
  #
  class CachingService

    # delegatee - the actual Bamboo::Service to be used to do the health checking
    #
    def initialize(delegatee)
      @delegatee = delegatee
    end

    # See Bamboo::Service.ensure_running.
    #
    def ensure_running
      if @last_result == :not_running
        fail("Bamboo service failed a previous health check.")
      else
        begin
          @delegatee.ensure_running
          @last_result = :running
        rescue => e
          @last_result = :not_running
          raise e
        end
      end
    end
  end

  class Service

    def initialize(client, configuration)
      @client = client
      @retries = configuration[:health_check_retries]
      @timeout = configuration[:health_check_timeout]
    end

    # Ensures the referenced Bamboo instance is up and running.
    #
    # If the service is down or cannot be reached for some reason, the method will wait up to the configured limit.
    # If the retry limit has been reached, the method will raise an exception.
    #
    def ensure_running
      for i in 1..@retries do
        result = @client.check_health
        case result.status
          when :not_running
            Puppet.debug("%s Waiting #{@timeout} seconds before trying again." % result.log_message)
            sleep(@timeout)
          when :running
              Puppet.debug("Bamboo service is running.")
            return
          when :broken
            fail(result.log_message)
          else
        end
      end
      fail("Bamboo service did not start up within #{@timeout * @retries} seconds. You should check the Bamboo log " +
        "files to see if something is wrong or consider increasing the timeout if the service is not starting up in " +
        "time.")
    end

    class Status
      attr_reader :status, :log_message

      def initialize(status, log_message)
        @status = status
        @log_message = log_message
      end

      # Service is still starting up ...
      #
      def self.not_running(log_message)
        Status.new(:not_running, log_message)
      end

      # Service is running.
      #
      def self.running
        Status.new(:running, '')
      end

      # Service failed to start up and and will not recover by waiting any longer.
      #
      def self.broken(log_message)
        Status.new(:broken, log_message)
      end
    end

    # Talks to the service in order to find out about the current health of the service.
    # Since bamboo does not have a health check API, we just hit the REST endpoint /rest/api/latest/info and check http return code
    class HealthCheckClient

      NOT_RUNNING_MSG = 'Bamboo service is not running, yet. Last reported status is: %s'
      CONFIGURATION_BROKEN_MSG = 'Bamboo service reached state %s from which it cannot recover from without user intervention. Reported error is: %s.'

      def initialize(configuration)
        @bamboo = Bamboo::Rest.new(configuration, configuration[:bamboo_base_url])
      end

      # Returns the health status of the service.
      #
      def check_health
        response = @bamboo.make_request('/rest/api/latest/info', 'get') do |req|
          req['Accept-Encoding'] = 'application/json'
        end
        case response.code
          when '200' then Service::Status.running
          when '500' then Service::Status.broken(response.to_s)
          else Service::Status.not_running(NOT_RUNNING_MSG % response.to_s)
        end
      end
    end
  end
end
