require 'json'
require 'puppet'
require 'yaml'
require "rexml/document"

module Bamboo
  class Config
    CONFIG_BAMBOO_BASE_URL = :bamboo_base_url
    CONFIG_ADMIN_USERNAME = :admin_username
    CONFIG_ADMIN_PASSWORD = :admin_password
    CONFIG_CROWD_USERNAME = :crowd_admin_username
    CONFIG_CROWD_PASSWORD = :crowd_admin_password
    CONFIG_CONNECTION_TIMEOUT = :connection_timeout
    CONFIG_CONNECTION_OPEN_TIMEOUT = :connection_open_timeout
    CONFIG_HEALTH_CHECK_RETRIES = :health_check_retries
    CONFIG_HEALTH_CHECK_TIMEOUT = :health_check_timeout
    CONFIG_BAMBOO_HOME_DIR = :bamboo_home_dir

    def self.configure
      @config ||= read_config

      # Depending on user repository type of bamboo(hibernate or crowd), the rest client need to use proper username/password
      user_repository_type = :hibernate
      if @config[CONFIG_BAMBOO_HOME_DIR]
        user_repository_type = get_user_repository_type("#{@config[CONFIG_BAMBOO_HOME_DIR]}/xml-data/configuration/atlassian-user.xml")
      end

      if user_repository_type == :crowd
        @config[CONFIG_ADMIN_USERNAME] = @config[CONFIG_CROWD_USERNAME]
        @config[CONFIG_ADMIN_PASSWORD] = @config[CONFIG_CROWD_PASSWORD]
      end

      yield @config[CONFIG_BAMBOO_BASE_URL], @config
    end

    # Returns: the full path to the file where this class sources its information from.
    #
    # Notice: any provider should have a soft dependency on this file to make sure it is created before usage.
    #
    def self.file_path
      @config_file_path ||= File.expand_path(File.join(Puppet.settings[:confdir], '/bamboo_rest.conf'))
    end

    def self.reset
      @config = nil
      @config_file_path = nil
    end

    # Read the config file
    def self.read_config
      begin
        Puppet::debug("Parsing configuration file #{file_path}")
        # each loop used to convert hash keys from String to Symbol; each doesn't return the modified hash ... ugly, I know
        config = Hash.new
        YAML.load_file(file_path).each{ |key, value| config[key.intern] = value}
      rescue => e
        raise Puppet::ParseError, "Could not parse YAML configuration file '#{file_path}': #{e}"
      end

      if config[CONFIG_BAMBOO_BASE_URL].nil?
        raise Puppet::ParseError, "Config file #{file_path} must contain a value for key '#{CONFIG_BAMBOO_BASE_URL}'."
      end

      if config[CONFIG_BAMBOO_BASE_URL].start_with?('http://') && !config[CONFIG_BAMBOO_BASE_URL].start_with?('http://localhost')
        Puppet.warning('You are sending bamboo password in plain text!')
      end

      if config[CONFIG_ADMIN_USERNAME].nil?
        raise Puppet::ParseError, "Config file #{file_path} must contain a value for key '#{CONFIG_ADMIN_USERNAME}'."
      end
      if config[CONFIG_ADMIN_PASSWORD].nil?
        raise Puppet::ParseError, "Config file #{file_path} must contain a value for key '#{CONFIG_ADMIN_PASSWORD}'."
      end

      {
        CONFIG_BAMBOO_BASE_URL         => config[CONFIG_BAMBOO_BASE_URL].chomp('/'),
        CONFIG_ADMIN_USERNAME          => config[CONFIG_ADMIN_USERNAME],
        CONFIG_ADMIN_PASSWORD          => config[CONFIG_ADMIN_PASSWORD],
        CONFIG_CROWD_USERNAME          => config[CONFIG_CROWD_USERNAME],
        CONFIG_CROWD_PASSWORD          => config[CONFIG_CROWD_PASSWORD],
        CONFIG_CONNECTION_TIMEOUT      => Integer(config.fetch(CONFIG_CONNECTION_TIMEOUT, 10)),
        CONFIG_CONNECTION_OPEN_TIMEOUT => Integer(config.fetch(CONFIG_CONNECTION_OPEN_TIMEOUT, 10)),
        CONFIG_HEALTH_CHECK_RETRIES    => Integer(config.fetch(CONFIG_HEALTH_CHECK_RETRIES, 50)),
        CONFIG_HEALTH_CHECK_TIMEOUT    => Integer(config.fetch(CONFIG_HEALTH_CHECK_TIMEOUT, 3)),
        CONFIG_BAMBOO_HOME_DIR         => config[CONFIG_BAMBOO_HOME_DIR],
      }
    end

    def self.get_user_repository_type(atlassian_user_xml)
      type = :hibernate
      if(File.file?(atlassian_user_xml))
        doc = read_xml_file(atlassian_user_xml)
        if doc.get_elements('/atlassian-user/repositories/hibernate').empty?
          type = :crowd
        end
      end
      type
    end

    def self.read_xml_file(file)
      file = File.new(file)
      REXML::Document.new file
    end
  end
end
