require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_security_settings) do
  @doc = 'Puppet type for security settings'

  newparam(:name, :namevar => true) do
    desc 'Name of the configuration (i.e. current).'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:read_only_external_user_management, :boolean => true) do
    desc 'Enable this option if you are connecting Bamboo to an external user management system and do not have update rights there.'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:sign_up_enabled) do
    desc 'This will allow users to sign up for an account to Bamboo.'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:sign_up_enabled_captcha) do
    desc 'Forces the user to enter a captcha code on signup'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:display_contact_details_enabled) do
    desc 'This will allow Bamboo user\'s contact details to be visible. Disabling this option will hide the email address, IM address, and the group the user is in.'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:restricted_administrator_role_enabled) do
    desc 'This will enable the restricted administrator role'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:brute_force_protection_enabled) do
    desc 'Forces the user to enter a captcha code if they meet the maximum amount of failed login attempts'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:brute_force_protection_login_attempts) do
    desc 'Number of login attempts before captcha is shown'

    validate do |value|
      raise ArgumentError, "Number of login attempts must be a non-negative integer, got #{value}" unless value.to_s =~ /\d+/
      raise ArgumentError, "Number of login attempts must bigger than or equal to one" unless value.to_i >= 1
    end
    munge { |value| Integer(value) }
  end

  newproperty(:xsrf_protection_enabled) do
    desc 'Prevents attackers from impersonating you and accessing Bamboo'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:xsrf_protection_disable_for_http_get) do
    desc 'This option is provided for compatibility reasons and will be removed in future releases. It practically disables XSRF protection and should not be used.'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:resolve_artifacts_content_type_by_extension_enabled) do
    desc 'Bamboo will serve your html artifacts as html, and let your browser browse to them. This is helpful but creates an XSS vulnerability that could be exploited by malicious users'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:sox_compliance_mode_enabled) do
    desc 'This will enable the SOX compliant user role'

    newvalue(:true)
    newvalue(:false)
  end

  validate do
    raise ArgumentError, "Can not set brute_force_protection_login_attempts if brute_force_protection_enabled is false or nil" if (self[:brute_force_protection_enabled].nil? || self[:brute_force_protection_enabled] == :false) && !self[:brute_force_protection_login_attempts].nil?
    raise ArgumentError, "Can not set sign_up_enabled_captcha if sign_up_enabled is false or nil" if (self[:sign_up_enabled].nil? || self[:sign_up_enabled] == :false) && !self[:sign_up_enabled_captcha].nil?
    raise ArgumentError, "Can not set xsrf_protection_disable_for_http_get if xsrf_protection_enabled is false or nil" if (self[:xsrf_protection_enabled].nil? || self[:xsrf_protection_enabled] == :false) && !self[:xsrf_protection_disable_for_http_get].nil?
  end
end