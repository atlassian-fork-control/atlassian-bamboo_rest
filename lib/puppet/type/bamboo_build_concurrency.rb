require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_build_concurrency) do
  @doc = 'Puppet type for bamboo build concurrency'

  newparam(:name, :namevar => true) do
    desc 'Name of the configuration (i.e. concurrent).'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:build_concurrency_enabled, :boolean => true) do
    desc 'Whether build concurrency is enabled'

    newvalue(:true)
    newvalue(:false)
  end


  newproperty(:default_concurrent_builds_number) do
    desc 'Default concurrent build number'

    validate do |value|
      raise ArgumentError, "Default concurrent build number must be a non-negative integer, got #{value}" unless value.to_s =~ /\d+/
      raise ArgumentError, "Default concurrent build number must bigger than or equal to one" unless value.to_i >= 1
    end

    munge { |value| Integer(value) }
  end
end