require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_quarantine) do
  @doc = 'Puppet Type for Bamboo Quarantine Configuration'

  newparam(:name, :namevar => true) do
    desc 'name of the configuration. E.g., current'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:quarantine_tests_enabled, :boolean => true) do
    desc 'Whether quarantine tests are enabled'

    newvalue(:true)
    newvalue(:false)
  end

end
