require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'hash_password.rb'))

Puppet::Type.newtype(:bamboo_shared_credential) do
  @doc = 'Puppet type to manage bamboo shared credentials'

  ensurable

  newparam(:name, :namevar=>true) do
    desc 'Name of the shared credential'
    # Bamboo currently does not enforce uniqueness of shared credential names. Raised BDEV-10069
  end

  newproperty(:ssh_key) do
    desc 'Ssh of ssh shared credential'

    validate do |value|
      raise ArgumentError, 'ssh_key cannot be empty' if value.to_s.empty?
    end

    # Changes the comparison function
    def insync?(is)
      if(is == :absent and @should[0] != :absent)
        return false
      elsif(is != :absent and @should[0] == :absent)
        return false
      elsif(is == :absent and @should[0] == :absent)
        return true
      else
        Bamboo::HashPassword.check_password_hash(is, @should[0])
      end
    end

    #Log messages are changed so that the password does not appear in the logs.
    def change_to_s(currentvalue, newvalue)
      if currentvalue == :absent
        return 'ssh_key has been created'
      else
        return 'ssh_key has been changed'
      end
    end

    def is_to_s( currentvalue )
      return '[old ssh_key hash redacted]'
    end

    def should_to_s( newvalue )
      return '[new ssh_key hash redacted]'
    end
  end

  newproperty(:ssh_passphrase) do
    desc 'Ssh passphrase of ssh key'

    # Changes the comparison function
    def insync?(is)
      if(is == :absent and @should[0] != :absent)
        return false
      elsif(is != :absent and @should[0] == :absent)
        return false
      elsif(is == :absent and @should[0] == :absent)
        return true
      else
        Bamboo::HashPassword.check_password_hash(is, @should[0])
      end
    end

    #Log messages are changed so that the password does not appear in the logs.
    def change_to_s(currentvalue, newvalue)
      if currentvalue == :absent
        return 'ssh passphrase has been created'
      else
        return 'ssh passphrase has been changed'
      end
    end

    def is_to_s( currentvalue )
      return '[old ssh passphrase hash redacted]'
    end

    def should_to_s( newvalue )
      return '[new ssh passphrase hash redacted]'
    end
  end
end