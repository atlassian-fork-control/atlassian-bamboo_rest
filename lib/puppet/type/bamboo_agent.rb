Puppet::Type.newtype(:bamboo_agent) do
  @doc = 'This type provides ability to enable/disable bamboo agents by types. E.g. you can enable/disable all remote/elastic/local agents.'

  newparam(:name, :namevar => true) do
    desc 'Specify the type of bamboo agents. Support 3 types of agents: remote, local and elastic.'

    newvalues(:remote, :local, :elastic)
  end

  newproperty(:enabled, :boolean => true) do
    desc 'Specify whether the type of agents should be enabled or not'

    newvalue(:true)
    newvalue(:false)
  end
end