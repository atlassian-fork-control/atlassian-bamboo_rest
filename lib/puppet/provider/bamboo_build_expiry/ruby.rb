require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))


Puppet::Type.type(:bamboo_build_expiry).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc 'Puppet provider to manage build expiry'

  # REST endpoint to manage bamboo build expiry
  def self.bamboo_resource
    '/rest/admin/latest/expiry/configuration'
  end

  def self.map_config_to_resource_hash(bamboo_build_expiry)
    res_hash = {}

    res_hash[:duration] = bamboo_build_expiry['duration'] unless bamboo_build_expiry['duration'].nil?
    res_hash[:period] = bamboo_build_expiry['period'].to_s.intern unless bamboo_build_expiry['period'].nil?
    res_hash[:builds_to_keep] = bamboo_build_expiry['buildsToKeep'] unless bamboo_build_expiry['buildsToKeep'].nil?
    res_hash[:deployments_to_keep] = bamboo_build_expiry['deploymentsToKeep'] unless bamboo_build_expiry['deploymentsToKeep'].nil?
    res_hash[:maximum_ignored_log_size] = bamboo_build_expiry['maximumIgnoredLogSize'] unless bamboo_build_expiry['maximumIgnoredLogSize'].nil?
    res_hash[:expire_results] = bamboo_build_expiry['expireResults'].to_s.intern unless bamboo_build_expiry['expireResults'].nil?
    res_hash[:expire_artifacts] = bamboo_build_expiry['expireArtifacts'].to_s.intern unless bamboo_build_expiry['expireArtifacts'].nil?
    res_hash[:expire_logs] = bamboo_build_expiry['expireLogs'].to_s.intern unless bamboo_build_expiry['expireLogs'].nil?
    res_hash[:labels_to_exclude] = bamboo_build_expiry['labelsToExclude'] unless bamboo_build_expiry['labelsToExclude'].nil?
    res_hash[:cron_expression] = bamboo_build_expiry['cronExpression'] unless bamboo_build_expiry['cronExpression'].nil?

    res_hash
  end

  def map_resource_hash_to_config
    config = {}

    config['duration'] = resource[:duration] unless resource[:duration].nil?
    config['period'] = resource[:period] unless resource[:period].nil?
    config['buildsToKeep'] = resource[:builds_to_keep] unless resource[:builds_to_keep].nil?
    config['deploymentsToKeep'] = resource[:deployments_to_keep] unless resource[:deployments_to_keep].nil?
    config['maximumIgnoredLogSize'] = resource[:maximum_ignored_log_size] unless resource[:maximum_ignored_log_size].nil?
    config['expireResults'] = resource[:expire_results] unless resource[:expire_results].nil?
    config['expireArtifacts'] = resource[:expire_artifacts] unless resource[:expire_artifacts].nil?
    config['expireLogs'] = resource[:expire_logs] unless resource[:expire_logs].nil?
    config['labelsToExclude'] = resource[:labels_to_exclude] unless resource[:labels_to_exclude].nil?
    config['cronExpression'] = resource[:cron_expression] unless resource[:cron_expression].nil?

    config
  end

  mk_resource_methods

end
