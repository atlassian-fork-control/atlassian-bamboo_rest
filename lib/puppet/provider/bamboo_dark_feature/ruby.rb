require 'json'
require 'puppet'
require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_ensurable_provider.rb'))

Puppet::Type.type(:bamboo_dark_feature).provide(:ruby, :parent => Puppet::Provider::BambooEnsurableProvider) do
  desc 'Puppet provider to manage Bamboo dynamic dark features'

  def self.bamboo_resource
    '/rest/admin/latest/darkFeatures/'
  end

  def self.map_config_to_resource_hash(dark_features_res)
    dark_features_res.collect do |dark_feature|
      new ({
        :name => dark_feature['key'],
        :ensure => :present,
        })
    end
  end

  mk_resource_methods

  def map_resource_hash_to_config
    {
      'key' => resource[:name],
    }
  end

  def name=(val)
    @dirty_flag = true
  end

  def create
    config = map_resource_hash_to_config
    endpoint = self.class.bamboo_resource + config['key'].to_s
    config['enabled'] = true
    Bamboo::Rest.update_bamboo_settings(endpoint, config)
  end

  def destroy
    # Bamboo does not DELETE the record, but PUTs it with enabled: false.
    config = map_resource_hash_to_config
    config['enabled'] = false
    endpoint = self.class.bamboo_resource + config['key'].to_s
    Bamboo::Rest.update_bamboo_settings(endpoint, config)
  end

  def flush
    if @dirty_flag
      config = map_resource_hash_to_config
      endpoint = self.class.bamboo_resource + config['key'].to_s
      Bamboo::Rest.update_bamboo_settings(endpoint, config)
    end
  end
end
