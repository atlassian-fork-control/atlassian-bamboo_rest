require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), 'bamboo_artifact_handler_provider.rb'))

Puppet::Type.type(:bamboo_artifact_handler).provide(:server_local, :parent => Puppet::Provider::BambooArtifactHandlerProvider) do
  desc 'Puppet provider for type server local artifact handler'

  confine :true => begin
            name.to_s.intern == :server_local
          end

  defaultfor :operatingsystem => [:debian, :darwin]

  def self.bamboo_resource
    '/rest/admin/latest/artifactHandlers/serverLocal'
  end

  def self.artifact_handler_type
    'server_local'
  end

  # ask puppet to create the get/set methods automatically
  mk_resource_methods

end