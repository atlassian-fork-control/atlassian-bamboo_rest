require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), 'bamboo_artifact_handler_provider.rb'))

Puppet::Type.type(:bamboo_artifact_handler).provide(:s3, :parent => Puppet::Provider::BambooArtifactHandlerProvider) do
  desc 'Puppet provider for type bamboo remote artifact handler'

  confine :true => begin
            name.to_s.intern == :s3
          end

  def self.bamboo_resource
    '/rest/admin/latest/artifactHandlers/s3'
  end

  def self.artifact_handler_type
    's3'
  end

  def self.map_config_to_resource_hash(artifact_handler_config)
    config = super(artifact_handler_config)
    if(!artifact_handler_config['attributes'].nil?)
      attributes = artifact_handler_config['attributes']
      config[:access_key_id] = attributes['accessKeyId'] unless attributes['accessKeyId'].nil?
      config[:secret_access_key] = attributes['secretAccessKey'] unless attributes['secretAccessKey'].nil?
      config[:bucket_name] = attributes['bucketName'] unless attributes['bucketName'].nil?
      config[:region] = attributes['region'].downcase.gsub('_','-') unless attributes['region'].nil?
      config[:bucket_path] = attributes['bucketPath'] unless attributes['bucketPath'].nil?
      config[:max_artifact_file_count] = attributes['maxArtifactFileCount'] unless attributes['maxArtifactFileCount'].nil?
    end
    config
  end

  def map_resource_hash_to_config
    config = super
    attributes = {}
    attributes['accessKeyId'] = resource[:access_key_id] unless resource[:access_key_id].nil?
    attributes['secretAccessKey'] = resource[:secret_access_key] unless resource[:secret_access_key].nil?
    attributes['bucketName'] = resource[:bucket_name] unless resource[:bucket_name].nil?
    attributes['region'] = resource[:region].upcase.gsub('-','_') unless resource[:region].nil?
    attributes['bucketPath'] = resource[:bucket_path] unless resource[:bucket_path].nil?
    attributes['maxArtifactFileCount'] = resource[:max_artifact_file_count].to_i unless resource[:max_artifact_file_count].nil?
    if(attributes.size > 0)
      config['attributes'] = attributes
    end
    config
  end

  # ask puppet to create the get/set methods automatically
  mk_resource_methods

end