require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', '..', 'puppet_x/bamboo/rest.rb'))

Puppet::Type.type(:bamboo_agent).provide(:ruby, :parent => Puppet::Provider) do
  desc 'Puppet type to enable/disable bamboo agents by type'

  def self.bamboo_resource
    '/rest/admin/latest/config/agents'
  end

  def self.instances
    agents = Bamboo::Rest.get_bamboo_settings(self.bamboo_resource)["results"]

    # This is a bit tricky. All agent types are default to disabled.
    # If one type of agent does not have any instance and enabled is set to true in manifest, puppet still thinks it is
    # currently disabled and will try to enable agents of that type. Though no actual http request will be sent.
    # If agents of one type are not all enabled, puppet will do nothing if enabled is set to true in manifest.
    # If agents of one type are all disabled, puppet will enable all the agents.
    # But if enabled is set to false in manifest, puppet will make sure the type of agents are all disabled.
    elastic_enabled = false.to_s.intern
    remote_enabled = false.to_s.intern
    local_enabled = false.to_s.intern
    agents.each do |agent|
      if agent['type'] == 'local' and agent['enabled'] == true
        local_enabled = true.to_s.intern
      end
      if agent['type'] == 'remote' and agent['enabled'] == true
        remote_enabled = true.to_s.intern
      end
      if agent['type'] == 'elastic' and agent['enabled'] == true
        elastic_enabled = true.to_s.intern
      end
    end

    [
      new(
        {
          :name    => :local,
          :enabled => local_enabled
        }
      ),
      new(
        {
          :name    => :remote,
          :enabled => remote_enabled
        }
      ),
      new(
        {
          :name    => :elastic,
          :enabled => elastic_enabled
        }
      )
    ]
  end

  def self.prefetch(resources)
    agent_config = self.instances
    resources.keys.each do |name|
      if provider = agent_config.find {|variable| variable.name == name}
        resources[name].provider = provider
      end
    end
  end

  mk_resource_methods

  def flush
    agents = Bamboo::Rest.get_bamboo_settings(self.class.bamboo_resource)["results"]
    enabled = resource[:enabled]
    agents.each do |agent|
      if agent['type'] == resource[:name].to_s and agent['enabled'] != resource[:enabled]
        Bamboo::Rest.update_bamboo_settings(self.class.bamboo_resource + '/' + agent['id'].to_s, {'enabled' => enabled})
      end
    end
  end

end
