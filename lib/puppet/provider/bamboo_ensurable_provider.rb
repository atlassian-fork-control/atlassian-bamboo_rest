require 'json'
require 'puppet'
require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'rest.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

class Puppet::Provider::BambooEnsurableProvider < Puppet::Provider
  desc 'Parent puppet provider to manage Bamboo ensurable resources'

  def self.bamboo_resource
    notice 'should be implemented by child'
    '/rest/admin/latest/idonotexist/'
  end
  # retrieve all bamboo resources via rest endpoint
  def self.instances
    existing_res = Bamboo::Rest.get_bamboo_settings(self.bamboo_resource)
    self.map_config_to_resource_hash(existing_res)
  end

  def self.prefetch(resources)
    existing_res = self.instances
    resources.keys.each do |name|
      if provider = existing_res.find {|variable| variable.name == name}
        resources[name].provider = provider
      end
    end
  end

  def initialize(value={})
    super(value)
    @dirty_flag = false
  end

  # map REST response to resource hash
  def self.map_config_to_resource_hash(bamboo_settings)
    notice('Need to be implemented')
  end

  # map resource hash to format accepted by REST resource
  def map_resource_hash_to_config
    notice('Need to be implemented')
  end

  def exists?
    @property_hash[:ensure] == :present
  end

  # add this method to make test easy
  def dirty_flag=(value)
    @dirty_flag = value
  end

  def create
    config = map_resource_hash_to_config
    Bamboo::Rest.create_bamboo_resource(self.class.bamboo_resource, config)
  end

  def destroy
    Bamboo::Rest.delete_bamboo_resource(self.class.bamboo_resource + @property_hash[:id].to_s)
  end

  def flush
    # need the dirty flag, otherwise flush will be invoked after create
    if @dirty_flag
      config = map_resource_hash_to_config
      Bamboo::Rest.update_bamboo_settings(self.class.bamboo_resource + @property_hash[:id].to_s, config)
    end
  end
end
