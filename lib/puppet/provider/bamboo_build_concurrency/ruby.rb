require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_build_concurrency).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc 'Puppet provider to manage bamboo build concurrency configuration'

  # REST endpoint to manage bamboo build concurrency
  def self.bamboo_resource
    '/rest/admin/latest/config/build/concurrency'
  end

  def self.map_config_to_resource_hash(build_concurrency)
    {
      :build_concurrency_enabled        => build_concurrency['buildConcurrencyEnabled'].to_s.intern,
      :default_concurrent_builds_number => build_concurrency['defaultConcurrentBuildsNumber']
    }
  end

  def map_resource_hash_to_config
    config = {}
    config['buildConcurrencyEnabled'] = resource[:build_concurrency_enabled] unless resource[:build_concurrency_enabled].nil?
    config['defaultConcurrentBuildsNumber'] = resource[:default_concurrent_builds_number] unless resource[:default_concurrent_builds_number].nil?

    config
  end

  # Ask puppet to handle attribute getter/setter methods
  mk_resource_methods

end
