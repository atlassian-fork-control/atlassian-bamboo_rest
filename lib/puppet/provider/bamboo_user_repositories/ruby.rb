require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_user_repositories).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc 'Puppet provider to manage bamboo user repositories'

  # REST endpoint to manage bamboo user repositories
  def self.bamboo_resource
    '/rest/admin/latest/userManagement/userRepositories'
  end

  def self.map_config_to_resource_hash(bamboo_user_repositories)
    {
      :type                    => bamboo_user_repositories['type'],
      :server_url              => bamboo_user_repositories['serverURL'],
      :application_name        => bamboo_user_repositories['applicationName'],
      :application_password    => bamboo_user_repositories['applicationPassword'],
      :cache_refresh_interval  => bamboo_user_repositories['cacheRefreshInterval']
    }
  end

  def map_resource_hash_to_config
    config = {}
    config['type'] = resource[:type] unless resource[:type].nil?
    config['serverURL'] = resource[:server_url] unless resource[:server_url].nil?
    config['applicationName'] = resource[:application_name] unless resource[:application_name].nil?
    config['applicationPassword'] = resource[:application_password] unless resource[:application_password].nil?
    config['cacheRefreshInterval'] = resource[:cache_refresh_interval] unless resource[:cache_refresh_interval].nil?

    config
  end

  # Ask puppet to handle attribute getter/setter methods
  mk_resource_methods

end
