require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_ensurable_provider.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', '..', 'puppet_x', 'bamboo', 'rest.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', '..', 'puppet_x', 'bamboo', 'exception.rb'))

Puppet::Type.type(:bamboo_im_server).provide(:ruby, :parent => Puppet::Provider::BambooEnsurableProvider) do
  @doc = 'Puppet provider to manage bamboo im server configuration'

  def self.bamboo_resource
    '/rest/admin/latest/config/imServer'
  end

  def self.instances
    existing_res = Bamboo::Rest.get_bamboo_settings(self.bamboo_resource) do |value|
      if value.is_a?(Net::HTTPNotFound)
        return []
      elsif
        Bamboo::ExceptionHandler.process(value) { |msg|
          raise "Could not get #{self.bamboo_resource}, #{msg} "
        }
      end
    end
    self.map_config_to_resource_hash(existing_res)
  end

  def self.map_config_to_resource_hash(bamboo_im_server)
    if(!bamboo_im_server['status-code'].nil? && bamboo_im_server['status-code'] == 404)
      return []
    end
    config = {
      :host   => bamboo_im_server['host'],
      :name   => 'current',
      :ensure => :present,
    }
    config[:port] = bamboo_im_server['port'] unless bamboo_im_server['port'].nil?
    config[:username] = bamboo_im_server['username'] unless bamboo_im_server['username'].nil?
    config[:password] = bamboo_im_server['password'] unless bamboo_im_server['password'].nil?
    config[:resource_name] = bamboo_im_server['resource'] unless bamboo_im_server['resource'].nil?
    config[:tls_enabled] = bamboo_im_server['requireTLSSSLConnection'].to_s.intern unless bamboo_im_server['requireTLSSSLConnection'].nil?

    [new(config)]
  end

  def map_resource_hash_to_config
    config = {}
    if(resource[:host].nil?)
      raise ArgumentError, 'host cannot be nil'
    end

    config['host'] = resource[:host]
    config['port'] = resource[:port] unless resource[:port].nil?
    config['username'] = resource[:username] unless resource[:username].nil?
    config['password'] = resource[:password] unless resource[:password].nil?
    config['resource'] = resource[:resource_name] unless resource[:resource_name].nil?
    config['requireTLSSSLConnection'] = resource[:tls_enabled] unless resource[:tls_enabled].nil?

    config
  end

  # ask puppet to create the get/set methods automatically
  mk_resource_methods

  def host=(val)
    @dirty_flag = true
  end

  def port=(val)
    @dirty_flag = true
  end

  def username=(val)
    @dirty_flag = true
  end

  def password=(val)
    @dirty_flag = true
  end

  def resource_name=(val)
    @dirty_flag = true
  end

  def tls_enabled=(val)
    @dirty_flag = true
  end

  def destroy
    Bamboo::Rest.delete_bamboo_resource(self.class.bamboo_resource)
  end

  def create
    config = map_resource_hash_to_config
    Bamboo::Rest.update_bamboo_settings(self.class.bamboo_resource, config)
  end

  def flush
    # need the dirty flag, otherwise flush will be invoked after create
    if @dirty_flag
      config = map_resource_hash_to_config
      Bamboo::Rest.update_bamboo_settings(self.class.bamboo_resource, config)
    end
  end

end
