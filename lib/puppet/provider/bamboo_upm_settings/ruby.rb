require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_upm_settings).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc 'Puppet provider to manage UPM settings'

  def self.bamboo_resource
    '/rest/plugins/latest/settings'
  end

  def self.content_type
      'application/vnd.atl.plugins+json'
  end

  def self.map_config_to_resource_hash(upm_settings)
    res_hash = {}
    upm_settings['settings'].collect do |variable|
      case variable['key']
        when 'pacDisabled'
          res_hash[:marketplace_connection_enabled] = (variable['value']==false).to_s.intern
        when 'requestsDisabled'
          res_hash[:request_addon_enabled] = (variable['value']==false).to_s.intern
        when 'emailDisabled'
          res_hash[:email_notification_enabled] = (variable['value']==false).to_s.intern
        when 'autoUpdateEnabled'
          res_hash[:auto_update_enabled] = variable['value'].to_s.intern
        when 'privateListingsEnabled'
          res_hash[:private_listings_enabled] = variable['value'].to_s.intern
      end
    end
    res_hash
  end

  def map_resource_hash_to_config
    config = {
      'settings' => []
    }
    if !resource[:marketplace_connection_enabled].nil?
      config['settings'].push({
        'key' => 'pacDisabled',
        'value' => (resource[:marketplace_connection_enabled].to_s.intern)==:false
      })
    end
    if !resource[:request_addon_enabled].nil?
      config['settings'].push({
        'key' => 'requestsDisabled',
        'value' => (resource[:request_addon_enabled].to_s.intern)==:false
      })
    end
    if !resource[:email_notification_enabled].nil?
      config['settings'].push({
        'key' => 'emailDisabled',
        'value' => (resource[:email_notification_enabled].to_s.intern)==:false
      })
    end
    if !resource[:auto_update_enabled].nil?
      config['settings'].push({
        'key' => 'autoUpdateEnabled',
        'value' => (resource[:auto_update_enabled].to_s.intern)==:true
      })
    end
    if !resource[:private_listings_enabled].nil?
      config['settings'].push({
        'key' => 'privateListingsEnabled',
        'value' => (resource[:private_listings_enabled].to_s.intern)==:true
      })
    end
    config
  end

  # Ask puppet to handle attribute getter/setter methods
  mk_resource_methods
end
